using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Tools;

using ICD.Catalogue.Controller;
using ICD.Catalogue.View.Interfaces;

namespace ICD.Catalogue.View
{
	[RequireComponent(typeof(Scrollbar))]
	public class Selector : MonoBehaviour
	{
		[SerializeField]
		private BooleanAutoProperty _isActive;
		public bool isActive
		{
			get => _isActive.value;
			internal set => _isActive.value = value;
		}

		[SerializeField]
		[SceneObjectsOnly]
		[Required]
		private ScrollRect _scroll;

		[SerializeField]
		[SceneObjectsOnly]
		[Required]
		private RectTransform _selectionHandler;

		[SerializeField]
		[ReadOnly]
		private Object _selectionBehaviourObject;

		private SelectionBehaviourInterface _selectionBehaviour;
		public SelectionBehaviourInterface selectionBehaviour
		{
			get => _selectionBehaviour;

			internal set
			{
				if (_selectionBehaviour == value)
					return;

				_selectionBehaviour = value;
				_selectionBehaviourObject = value as Object;
			}
		}

		[SerializeField]
		[ReadOnly]
		private Object _currentSelectionItemObject;

		private SelectableItemInterface _currentSelectionItem;
		private SelectableItemInterface currentSelectionItem
		{
			get => _currentSelectionItem;
			set
			{
				_currentSelectionItem = value;
				_currentSelectionItemObject = value as Object;
			}
		}

		[SerializeField]
		private BooleanAutoProperty _isVisible;
		public bool isVisible
		{
			get => _isVisible.value;
			internal set => _isVisible.value = value;
		}

		void HandleRightClick()
		{
			_currentSelectionItem?.Select();
		}

		void Update()
		{
			if (_selectionBehaviour == null)
			{
				_currentSelectionItem = null;
				return;
			}

			if (_isActive.value)
			{
				_currentSelectionItem =
					_selectionBehaviour.Process(
						_selectionHandler,
						_scroll);
			}
			else
			{
				selectionBehaviour.ProcessInaction(
					_selectionHandler,
					_scroll);
				_currentSelectionItem = null;
			}
		}

		void OnEnable()
		{
			InputHandler.instance.RightClickActionPerformed += HandleRightClick;
		}

		void OnDisable()
		{
			InputHandler.instance.RightClickActionPerformed -= HandleRightClick;
		}
	}
}