
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ICD.Engine.Core.Singletons
{
	[ExecuteAlways]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Singleton))]
	internal class InstanceChecker : MonoBehaviour
	{
		internal Singleton singleton { private get; set; }
		void SceneUnloadedHandler(Scene scene)
		{
			if (singleton && !singleton.dontDestroyOnLoad)
#if UNITY_EDITOR
				if (!Application.isPlaying)
					DestroyImmediate(singleton.gameObject);
				else
#endif
					Destroy(singleton.gameObject);
		}
		void Awake()
		{
			SceneManager.sceneUnloaded += SceneUnloadedHandler;
		}
		void Start()
		{
			singleton = GetComponent<Singleton>();
			singleton?.OnInstanceStart();
		}
		void OnDestroy()
		{
			singleton?.OnInstanceDestroy();
			SceneManager.sceneUnloaded -= SceneUnloadedHandler;
		}
	}
}
