using System;

using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace ICD.Catalogue.View.Screens
{
	[DisallowMultipleComponent]
	public class PresentationScreen : MonoBehaviour
	{
		[SerializeField]
		[ReadOnly]
		private bool _isActive;
		public bool isActive
		{
			get => _isActive;
			internal set
			{
				if (_isActive == value)
					return;

				ActivationStateChanged?.Invoke(
					value);

				_isActive = value;
			}
		}

		[SerializeField]
		private BooleanEventsComposition _activationStateChanged;

		[SerializeField]
		private UnityEvent _transitionInvoked;

		public event Action<bool> ActivationStateChanged;
		public event Action TransitionInvoked;

		public PresentationScreen()
		{
			ActivationStateChanged +=
				newState =>
					_activationStateChanged?.Invoke(
						newState);
			TransitionInvoked +=
				() => _transitionInvoked?.Invoke();
		}

		public void ToScreen(PresentationScreen screenPrefab)
		{
			isActive = false;
			Instantiate(
				screenPrefab,
				transform.parent);
			TransitionInvoked?.Invoke();
		}
	}
}