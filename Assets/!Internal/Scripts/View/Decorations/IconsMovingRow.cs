using System.Collections.Generic;

using UnityEngine;
using Sirenix.OdinInspector;

namespace ICD.Catalogue.View.Decaorations
{
	public class IconsMovingRow : MonoBehaviour
	{
		[SerializeField]
		[Required]
		private string
			_appearanceTriggerName = "appearance";

		[SerializeField]
		[Required]
		[SceneObjectsOnly]
		private Transform _iconsAnchor;

		private Queue<Animator> _childrenAnimators =
			new Queue<Animator>();

		void Awake()
		{
			for (int c = _iconsAnchor.childCount - 1; c >= 0; c--)
			{
				var childAnimator = _iconsAnchor.GetChild(c).GetComponent<Animator>();
				if (childAnimator != null)
					_childrenAnimators.Enqueue(
						childAnimator);
			}
		}

		public void Swap()
		{
			var childAnimator = _childrenAnimators.Dequeue();
			_childrenAnimators.Enqueue(
				childAnimator);
			childAnimator.transform.SetSiblingIndex(0);
			childAnimator.SetTrigger(
				_appearanceTriggerName);
		}
	}
}