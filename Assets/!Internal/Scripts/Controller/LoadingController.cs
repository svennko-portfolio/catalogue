using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.SceneManagement;
using Sirenix.OdinInspector;

using ICD.Catalogue.Model;

namespace ICD.Catalogue.Controller
{
	[DisallowMultipleComponent]
	public class LoadingController : MonoBehaviour
	{
#if UNITY_EDITOR
		[SerializeField]
		[Required]
		[ValidateInput("SetSceneNameFromInspector")]
		private UnityEditor.SceneAsset _sceneBeingLoadedAfterDataLoading;

		bool SetSceneNameFromInspector(UnityEditor.SceneAsset scene)
		{
			if (scene != null)
			{
				_nameOfSceneBeingLoadedAfterDataLoading = scene.name;
				return true;
			}
			else
			{
				_nameOfSceneBeingLoadedAfterDataLoading = string.Empty;
				return false;
			}
		}
#endif
		[SerializeField]
		[ReadOnly]
		private string _nameOfSceneBeingLoadedAfterDataLoading;

		private CancellationTokenSource _lifeCancellation = new CancellationTokenSource();

		async void Awake()
		{
			if (ModelAgregator.instance.isLoadingNow)
				while (ModelAgregator.instance.isLoadingNow)
					await Task.Yield();
			else
				await ModelAgregator.instance.LoadData(
					_lifeCancellation.Token);

			SceneManager.LoadScene(
				_nameOfSceneBeingLoadedAfterDataLoading);
		}

		void OnDestroy()
		{
			_lifeCancellation?.Cancel();
		}
	}
}