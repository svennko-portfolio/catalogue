using UnityEngine;
using Sirenix.OdinInspector;

namespace ICD.Engine.Core.Animations
{
	[RequireComponent(typeof(Animator))]
	public abstract class AnimatorParameterSetter<ParameterType> : MonoBehaviour
		where ParameterType : struct
	{
		[SerializeField]
		[Required]
		private string _parameterName;
		public string parameterName => _parameterName;

		[SerializeField]
		[ValidateInput("SetValueFromInspector")]
		private ParameterType _parameterValue;

#if UNITY_EDITOR
		bool SetValueFromInspector(ParameterType newValue)
		{
			parameterValue = newValue;
			return true;
		}
#endif

		public ParameterType parameterValue
		{
			get
			{
				_parameterValue = GetParameterFromAnimator();
				return _parameterValue;
			}
			set
			{
				if (enabled)
					SetParameter(
						value);

				_parameterValue = value;
			}
		}

		[SerializeField]
		private bool _autoInitializeOnEnable;

		private Animator _animator;
		protected Animator animator
		{
			get
			{
				if (!_animator)
					_animator = GetComponent<Animator>();
				return _animator;
			}
		}

		protected abstract ParameterType GetParameterFromAnimator();
		protected abstract void SetParameter(ParameterType parameterValue);
		void OnEnable()
		{
			if (_autoInitializeOnEnable)
				parameterValue = _parameterValue;
			else
				_parameterValue = GetParameterFromAnimator();
		}

		void Update()
		{
			SetParameter(
				_parameterValue);
		}
	}
}
