
using UnityEngine;
using UnityEngine.Events;

namespace ICD.Engine.Core.Utility
{
    [RequireComponent(typeof(Animator))]
    public class AnimationEventsHandler : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent[] _eventsHandlers;
        public void InvokeHandler(int eventIndex)
		{
            if (_eventsHandlers == null || eventIndex < 0 || eventIndex >= _eventsHandlers.Length)
                return;
            _eventsHandlers[eventIndex]?.Invoke();
		}
        public void InokeAll()
		{
            if (_eventsHandlers == null)
                return;
            foreach (var handler in _eventsHandlers)
                handler?.Invoke();
		}
    }
}
