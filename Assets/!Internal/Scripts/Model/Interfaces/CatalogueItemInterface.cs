namespace ICD.Catalogue.Model.Interfaces
{
	public interface CatalogueItemInterface
	{
		public CatalogueItemMainDataInterface mainData { get; }
		CatalogueItemLocalizedDataInterface localizedData { get; }
	}
}