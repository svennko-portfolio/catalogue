using System;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Singletons;

namespace ICD.Catalogue.Controller
{
	public sealed class InputHandler : Singleton<InputHandler>
	{
		[FoldoutGroup("Input Actions")]
		[SerializeField]
		[Required]
		private InputActionReference
			_horizontalMovementAction,
			_verticalMovementAction,
			_otherActions;

		[SerializeField]
		UnityEvent
			_leftClickActionPerformed,
			_rightClickActionPerformed,
			_anyActionPerformed;

		[SerializeField]
		FloatEvent _verticalMovementActionPerformed;

		[SerializeField]
		[Min(0)]
		private float
			_horizontalThreshold,
			_verticalThreshold;

		[SerializeField]
		[ReadOnly]
		private float _maxHorisontalHandledValue;

		public event Action
			LeftClickActionPerformed,
			RightClickActionPerformed;
		public event Action<float> VerticalMovementActionPerformed;
		public event Action AnyActionPerformed;

		public override bool dontDestroyOnLoad => false;

		public float verticalMovementActionValue
		{
			get
			{
				if (!_verticalMovementAction)
					return 0;

				var value = _verticalMovementAction.action.ReadValue<float>();
				return
					MathF.Abs(value) > _verticalThreshold ?
						value :
						0;
			}
		}

		public InputHandler()
		{
			LeftClickActionPerformed += () => _leftClickActionPerformed?.Invoke();
			RightClickActionPerformed += () => _rightClickActionPerformed?.Invoke();
			VerticalMovementActionPerformed += value => _verticalMovementActionPerformed?.Invoke(value);
			AnyActionPerformed += () => _anyActionPerformed?.Invoke();
		}

		void OnEnable()
		{
			if (_horizontalMovementAction)
			{
				_horizontalMovementAction.action.Enable();
				_horizontalMovementAction.action.performed += HandleHorizontalMovementActionPerfomance;
				_horizontalMovementAction.action.canceled += HandleHorizontalMovementActionCanceling;
			}

			if (_verticalMovementAction)
			{
				_verticalMovementAction.action.Enable();
				_verticalMovementAction.action.performed += HandleVerticalMovementActionPerfomance;
			}

			if (_otherActions)
			{
				_otherActions.action.Enable();
				_otherActions.action.started += HandleOtherActionsStart;
			}
		}

		void OnDisable()
		{
			if (_horizontalMovementAction)
			{
				_horizontalMovementAction.action.Disable();
				_horizontalMovementAction.action.performed -= HandleHorizontalMovementActionPerfomance;
				_horizontalMovementAction.action.canceled -= HandleHorizontalMovementActionCanceling;
			}

			if (_verticalMovementAction)
			{
				_verticalMovementAction.action.Disable();
				_verticalMovementAction.action.performed -= HandleVerticalMovementActionPerfomance;
			}

			if (_otherActions)
			{
				_otherActions.action.Dispose();
				_otherActions.action.started -= HandleOtherActionsStart;
			}
		}

		void HandleVerticalMovementActionPerfomance(InputAction.CallbackContext callbackContext)
		{
			var value = callbackContext.ReadValue<float>();

			if (Mathf.Abs(value) > _verticalThreshold)
				VerticalMovementActionPerformed?.Invoke(
					value);

			AnyActionPerformed?.Invoke();
		}
		void HandleHorizontalMovementActionPerfomance(InputAction.CallbackContext callbackContext)
		{
			var value = callbackContext.ReadValue<float>();
			if (Mathf.Abs(value) > Mathf.Abs(_maxHorisontalHandledValue))
				_maxHorisontalHandledValue = value;

			AnyActionPerformed?.Invoke();
		}
		void HandleHorizontalMovementActionCanceling(InputAction.CallbackContext callbackContext)
		{
			if (Math.Abs(_maxHorisontalHandledValue) < _horizontalThreshold)
				return;

			if (_maxHorisontalHandledValue > 0)
				RightClickActionPerformed?.Invoke();
			else
				LeftClickActionPerformed?.Invoke();

			_maxHorisontalHandledValue = 0;

			AnyActionPerformed?.Invoke();
		}
		void HandleOtherActionsStart(InputAction.CallbackContext callbackContext) =>
			AnyActionPerformed?.Invoke();
	}
}