using UnityEngine;
using ICD.Engine.Core.Utility;

namespace ICD.Catalogue.Controller
{
	public static class ApplizationInitializer
	{
		public const string APPLICATION_SETTINGS_GROUP = "APPLICATION";

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
		static void Initialize()
		{
			INISetting.ReloadINI();

			SetScreen();
			SetCursor();
		}

		static void SetScreen()
		{
			Screen.SetResolution(
				INISetting.GetValueWithAdd(
					APPLICATION_SETTINGS_GROUP,
					"Froce Resolution. Wifth",
					1080),
				INISetting.GetValueWithAdd(
					APPLICATION_SETTINGS_GROUP,
					"Froce Resolution. Height",
					1920),
				true);
		}

		static void SetCursor()
		{
			Cursor.visible =
				INISetting.GetValueWithAdd(
					APPLICATION_SETTINGS_GROUP,
					"Cursor Visibility",
					true);
		}
	}
}
