

using UnityEngine;

namespace ICD.Engine.Core.Utility
{
	public static class AnimationCurveHelper
	{
		public struct CurveKey
		{
			public Keyframe frame { get; set; }
			public uint index { get; set; }
			public CurveKey(Keyframe frame, uint index)
			{
				this.frame = frame;
				this.index = index;
			}
		}
		public struct CurveProperties
		{
			private Keyframe _startKey;
			public Keyframe startKey => _startKey;

			private Keyframe _endKey;
			public Keyframe endKey => _endKey;

			public float duration => _endKey.time - _startKey.time;
			public float durationFromZero => _endKey.time;


			private Keyframe _minValueKey;
			public Keyframe minValueKey => _minValueKey;

			private Keyframe _maxValueKey;
			public Keyframe maxValueKey => _maxValueKey;

			public float valueDelta => _maxValueKey.value - _minValueKey.value;

			void SetProperties(Keyframe[] keys)
			{
				if (keys == null || keys.Length == 0)
					return;

				_startKey = keys[0];
				_endKey = keys[keys.Length - 1];
				_minValueKey.value = float.PositiveInfinity;
				_maxValueKey.value = float.NegativeInfinity;

				for (int k = 0; k < keys.Length; k++)
				{
					if (keys[k].value < _minValueKey.value)
						_minValueKey = keys[k];

					if (keys[k].value > _maxValueKey.value)
						_maxValueKey = keys[k];
				}
			}
			public CurveProperties(AnimationCurve curve)
			{
				_startKey =
				_endKey =
				_minValueKey =
				_maxValueKey = new Keyframe();

				if (curve == null)
					return;

				SetProperties(curve.keys);
			}
			public CurveProperties(Keyframe[] keys)
			{
				_startKey =
				_endKey =
				_minValueKey =
				_maxValueKey = new Keyframe();

				SetProperties(keys);
			}
		}

		public static CurveProperties GetProperties(this AnimationCurve curve) =>
			new CurveProperties(curve);
		public static CurveProperties GetCurveProperties(this Keyframe[] keys) =>
			new CurveProperties(keys);
	}
}
