namespace ICD.Engine.Core.Animations
{
	public sealed class AnimatorIntegerSetter : AnimatorParameterSetter<int>
	{
		protected override int GetParameterFromAnimator() =>
			animator.GetInteger(
				parameterName);
		protected override void SetParameter(int parameterValue)
		{
			animator.SetInteger(
				parameterName,
				parameterValue);
		}
	}
}
