namespace ICD.Catalogue.Model.Interfaces
{
    public interface ProductInterface : CatalogueItemInterface
    {
        public new ProductMainDataInterface mainData { get; }
        public new ProductLocalizedDataInterface localizedData { get; }
    }
}