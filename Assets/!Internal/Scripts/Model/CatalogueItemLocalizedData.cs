using System;

using UnityEngine;
using Newtonsoft.Json;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model
{
	[Serializable]
	public abstract class CatalogueItemLocalizedData : CatalogueItemLocalizedDataInterface
	{
		[SerializeField]
		private string _name;

		[JsonProperty("name", Order = 1)]
		public string name
		{
			get => _name;
			internal set => _name = value;
		}

		[SerializeField]
		[Multiline]
		private string _shortDescription;

		[JsonProperty("short_description", Order = 2)]
		public string shortDescription
		{
			get => _shortDescription;
			internal set => _shortDescription = value;
		}

		[SerializeField]
		private Sprite _logo;

		[JsonIgnore]
		public Sprite logo
		{
			get => _logo;
			internal set => _logo = value;
		}

		Sprite CatalogueItemLocalizedDataInterface.logo
		{
			get => _logo;
			set => _logo = value;
		}

		[SerializeField]
		[JsonProperty("logo_URL", Order = 3)]
		private string _logoURL;
		internal string logoURL
		{
			get => _logoURL;
			set => _logoURL = value;
		}

		string CatalogueItemLocalizedDataInterface.logoURL => _logoURL;

		[SerializeField]
		[JsonProperty("hide_name_in_header")]
		private bool _hideNameInHeader;

		[JsonIgnore]
		public bool hideNameInHeader
		{
			get => _hideNameInHeader;
			internal set => _hideNameInHeader = value;
		}

		public override string ToString() =>
			$"{nameof(CatalogueItemMainData)} " +
			$"{{ " +
				$"{nameof(name)}: {name}; " +
				$"{nameof(shortDescription)}: {shortDescription}; " +
				$"{nameof(logoURL)}: {logoURL}; " +
			$"}} ";
	}
}