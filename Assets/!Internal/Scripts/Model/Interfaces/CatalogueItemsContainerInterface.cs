﻿using System.Collections.Generic;

namespace ICD.Catalogue.Model.Interfaces
{
	public interface CatalogueItemsContainerInterface
	{
		public IEnumerable<CatalogueItemInterface> all { get; }
		public CategoryInterface GetCategory(uint categoryID);
		public ProductInterface GetProduct(uint productID);
		public IReadOnlyList<CatalogueItemInterface> GetCategoryChildren(uint? parentCategoryID);
	}

	internal interface WritableCatalogueItemsContainerInterface : CatalogueItemsContainerInterface
	{
		public bool TryAddCatalogueItem(CatalogueItemInterface catalogueItem, bool overwriteIfExists = true);
		public bool TryAddCatalogueItems(IEnumerable<CatalogueItemInterface> catalogueItems, bool overwriteIfExists = true);
		public bool TryAddProduct(ProductInterface product, bool overwriteIfExists = true);
		public bool TryAddProducts(IEnumerable<ProductInterface> products, bool overwriteIfExists = true);
		public bool TryAddCategory(CategoryInterface category, bool overwriteIfExists = true);
		public bool TryAddCategories(IEnumerable<CategoryInterface> categories, bool overwriteIfExists = true);
		public void ClearData();
	}
}