using System;

using UnityEngine;
using Sirenix.OdinInspector;

namespace ICD.Engine.Core.Tools
{
	public abstract class AutoProperty<ValueType>
		where ValueType : IEquatable<ValueType>
	{
		[SerializeField]
		[ValidateInput("SetValueFromInspector")]
		private ValueType _value;

#if UNITY_EDITOR
		private ValueType _validatedVisibilityState;
		bool SetValueFromInspector(ValueType newValue)
		{
			if (!newValue.Equals(_validatedVisibilityState))
			{
				_value = _validatedVisibilityState;
				value = newValue;
				_validatedVisibilityState = _value;
			}

			return true;
		}
#endif

		public ValueType value
		{
			get => _value;
			set => SetValue(value);
		}

		public event Action<ValueType> ValueChanged;

		public void SetWithoutNotification(ValueType value) =>
			SetValue(
				value,
				false);

		void SetValue(ValueType value, bool notificate = true)
		{
			if (_value.Equals(value))
				return;

			_value = value;

			if (notificate)
				ValueChanged.Invoke(
					value);
		}
	}
}