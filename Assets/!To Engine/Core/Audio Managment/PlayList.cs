
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

namespace ICD.Engine.Core.Audio
{
	[CreateAssetMenu(fileName = "Play List", menuName = "ICD/Engine/Audio/Play List")]
	public class PlayList : ScriptableObject, IList<AudioClip>
	{
		[SerializeField]
		private AudioMixerGroup _mixerGroup;
		public AudioMixerGroup mixerGroup => _mixerGroup;

		[SerializeField]
		private AnimationCurve
			_inVolumeCurve,
			_outVolumeCurve;
		public AnimationCurve inVolumeCurve => _inVolumeCurve;
		public AnimationCurve outVolumeCurve => _outVolumeCurve;

		[SerializeField]
		private List<AudioClip> _clips = new List<AudioClip>();

		//private static Exception _readOnlyException =
		//	new NotImplementedException(
		//		"Collection of AudioCLips is \"read only\"");

		public bool IsReadOnly => false;
		public int Count => _clips.Count;

		public AudioClip this[int index]
		{
			get => _clips[index];
			set => _clips[index] = value;
		}
		public bool Contains(AudioClip clip) =>
			_clips.Contains(clip);
		public void CopyTo(AudioClip[] array, int arrayIndex) =>
			_clips.CopyTo(array, arrayIndex);
		public void Add(AudioClip clip) =>
			_clips.Add(clip);
		public void Clear() =>
			_clips.Clear();
		public bool Remove(AudioClip clip) =>
			_clips.Remove(clip);
		public int IndexOf(AudioClip clip) =>
			_clips.IndexOf(clip);
		public void Insert(int index, AudioClip clip) =>
			_clips.Insert(
				index,
				clip);
		public void RemoveAt(int index) =>
			_clips.RemoveAt(index);

		public IEnumerator<AudioClip> GetRandomEnumerator()
		{
			var indexes = new List<int>(_clips.Count) { 0 };
			for (int index = 1; index < _clips.Count; index++)
				indexes.Insert(
					UnityEngine.Random.Range(
						0,
						indexes.Count),
					index);

			foreach (var index in indexes)
				yield return _clips[index];
		}
		public IEnumerator<AudioClip> GetDirectOrederEnumerator()
		{
			for (int c = 0; c < _clips.Count; c++)
				yield return _clips[c];
		}
		public IEnumerator<AudioClip> GetReversedtOrederEnumerator()
		{
			for (int c = _clips.Count - 1; c >= 0; c--)
				yield return _clips[c];
		}
		public IEnumerator<AudioClip> GetEnumerator(PlayMode mode = PlayMode.directOrder)
		{
			IEnumerator<AudioClip> enumerator = null;
			switch (mode)
			{
				default:
				case PlayMode.directOrder:
					enumerator = GetDirectOrederEnumerator();
					break;
				case PlayMode.recerseOrder:
					enumerator = GetReversedtOrederEnumerator();
					break;
				case PlayMode.random:
					enumerator = GetRandomEnumerator();
					break;
			}

			if (enumerator == null)
				yield break;

			do
				yield return enumerator.Current;
			while (enumerator.MoveNext());
		}
		public IEnumerator<AudioClip> GetEnumerator() =>
			_clips.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() =>
			GetEnumerator();
	}
}
