using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Localization;
using Sirenix.OdinInspector;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using ICD.Catalogue.Model.Interfaces;
using ICD.Catalogue.Model.DataLoading.Interfaces;

namespace ICD.Catalogue.Model.DataLoading
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Data/API/Local Data Base API", fileName = "Local Data Base API")]
	internal class LocalDataBaseAPI : ScriptableObject, DataBaseAPI
	{
		[SerializeField]
		[ValidateInput("InitializeFullDataDirectoryPath")]
		private string _dataDirectoryPath = string.Empty;

		private string _fullDataDirectoryPath;
		private string fullDataDirectoryPath
		{
			get
			{
				if (string.IsNullOrWhiteSpace(_fullDataDirectoryPath))
					InitializeFullDataDirectoryPath(
						_dataDirectoryPath);

				return _fullDataDirectoryPath;
			}
		}

		public bool isLoggedIn { get; private set; }


#if UNITY_EDITOR
		[FoldoutGroup("Editor Only")]
		[SerializeField]
		[ValidateInput("InitializeFullDataDirectoryPath")]
		private string _rootPath;
#endif

		bool InitializeFullDataDirectoryPath(string newPathPart)
		{
			var pathParts =
				new List<string>()
				{
					Application.dataPath
				};

#if UNITY_EDITOR
			if (!string.IsNullOrWhiteSpace(_rootPath))
				pathParts.Add(
					_rootPath);
#endif

			if (!string.IsNullOrWhiteSpace(_dataDirectoryPath))
				pathParts.Add(
					_dataDirectoryPath);

			_fullDataDirectoryPath =
				Path.Combine(
					pathParts.ToArray());

			return true;
		}

		public Task<bool> LogIn(string username, string password, CancellationToken cancellationToken = default) =>
			LogIn(
				new DataBaseCredentials()
				{
					username = username,
					password = password,
				},
				cancellationToken);

		public Task<bool> LogIn(DataBaseCredentials credentials, CancellationToken cancellationToken = default)
		{
			isLoggedIn = true;
			return
				Task.FromResult(
					true);
		}

		public Task<bool> LogOut(CancellationToken cancellationToken = default)
		{
			isLoggedIn = false;
			return
				Task.FromResult(
					true);
		}

		[SerializeField]
		private string
			_localesRelativePath = "Locales",
			_categoriesMainDataRelativePath = "Categories",
			_productsMainDataRelativePath = "Products",
			_categoriesLocalizedDataRelativePath = "Categories Localized Data",
			_productsLocalizedDataRelativePath = "Products Localized Data";

		public async Task<IEnumerable<CategoryMainDataInterface>> GetAllCategories(CancellationToken cancellationToken = default) =>
			await DeserializeData(
				Path.Combine(
					fullDataDirectoryPath,
					_categoriesMainDataRelativePath),
				new CategoryMainData[]
				{
					new CategoryMainData()
					{
						ID = 1,
						parentID = null,
						orderNumber = 1,
						iconURL=string.Empty,
						contentType = CategoryMainDataInterface.ContentType.products,
					}
				});

		public async Task<CategoryMainDataInterface> GetCategoriy(uint categoryID, CancellationToken cancellationToken = default) =>
			(await GetAllCategories()).
			FirstOrDefault(
				category => category.ID == categoryID);

		public async Task<IEnumerable<ProductMainDataInterface>> GetAllProducts(CancellationToken cancellationToken = default) =>
			await DeserializeData(
				Path.Combine(
					fullDataDirectoryPath,
					_productsMainDataRelativePath),
				new ProductMainData[]
				{
					new ProductMainData()
					{
						ID = 1,
						parentID = 1,
						orderNumber = 1,
						iconURL = string.Empty
					}
				});

		public async Task<ProductMainDataInterface> GetProduct(uint productID, CancellationToken cancellationToken = default) =>
			(await GetAllProducts()).
			FirstOrDefault(
				producr => producr.ID == productID);

		public async Task<IReadOnlyDictionary<string, CategoryLocalizedDataInterface>> GetCategoryLocalizedData(uint categoryID, CancellationToken cancellationToken = default)
		{
			var result =
				new Dictionary<string, CategoryLocalizedDataInterface>(
					LocalizationHelper.availableLocalesLocalesCount);

			foreach (var localizedDataPair in await GetAllCategoriesLocalizedData())
				if (localizedDataPair.Key.localeID.IsAvailableLocaleID() &&
					localizedDataPair.Key.catalogueItemID == categoryID &&
					!result.ContainsKey(localizedDataPair.Key.localeID))
					result.Add(
						localizedDataPair.Key.localeID,
						localizedDataPair.Value);

			return result;
		}

		public async Task<CategoryLocalizedDataInterface> GetCategoryLocalizedData(uint categoryID, string localeID, CancellationToken cancellationToken = default) =>
			(await GetCategoryLocalizedData(
				categoryID,
				cancellationToken)).
			FirstOrDefault(
				pair => pair.Key == localeID).Value;

		public async Task<IReadOnlyDictionary<string, ProductLocalizedDataInterface>> GetProductLocalizedData(uint productID, CancellationToken cancellationToken = default)
		{
			var result =
				new Dictionary<string, ProductLocalizedDataInterface>(
					LocalizationHelper.availableLocalesLocalesCount);

			foreach (var localizedDataPair in await GetAllProductsLocalizedData(cancellationToken))
				if (localizedDataPair.Key.localeID.IsAvailableLocaleID() &&
					localizedDataPair.Key.catalogueItemID == productID &&
					!result.ContainsKey(localizedDataPair.Key.localeID))
					result.Add(
						localizedDataPair.Key.localeID,
						localizedDataPair.Value);

			return result;
		}

		public async Task<ProductLocalizedDataInterface> GetProductLocalizedData(uint productID, string localeID, CancellationToken cancellationToken = default) =>
			(await GetProductLocalizedData(
				productID,
				cancellationToken)).
			FirstOrDefault(
				pair => pair.Key == localeID).Value;

		async Task<IReadOnlyDictionary<LocalizedDataKey, CategoryLocalizedDataInterface>> GetAllCategoriesLocalizedData(CancellationToken cancellationToken = default)
		{
			var localizedPairs =
				await DeserializeData(
					Path.Combine(
						fullDataDirectoryPath,
						_categoriesLocalizedDataRelativePath),
						new Dictionary<LocalizedDataKey, CategoryLocalizedData>()
						{
							{
								new LocalizedDataKey()
								{
									catalogueItemID = 1,
									localeID = LocalizationHelper.currentLocaleID
								},
								new CategoryLocalizedData()
								{
									name = string.Empty,
									shortDescription = string.Empty,
									logoURL = string.Empty
								}
							}
						}.ToArray(),
					cancellationToken);

			var result =
				new Dictionary<LocalizedDataKey, CategoryLocalizedDataInterface>(
					localizedPairs.Length);

			foreach (var pair in localizedPairs)
				if (!result.ContainsKey(pair.Key))
					result.Add(
						pair.Key,
						pair.Value);

			return result;
		}

		async Task<IReadOnlyDictionary<LocalizedDataKey, ProductLocalizedDataInterface>> GetAllProductsLocalizedData(CancellationToken cancellationToken = default)
		{
			var localizedPairs =
				await DeserializeData(
				Path.Combine(
					fullDataDirectoryPath,
					_productsLocalizedDataRelativePath),
					new Dictionary<LocalizedDataKey, ProductLocalizedData>()
						{
							{
								new LocalizedDataKey()
								{
									catalogueItemID = 1,
									localeID = LocalizationHelper.currentLocaleID
								},
								new ProductLocalizedData()
								{
									name = string.Empty,
									shortDescription = string.Empty,
									fullDescription = string.Empty,
									logoURL = string.Empty,
									photosURLs = new string[0],
									videosURLs = new string[0],
									productWebsiteQRCodeURL = string.Empty,
									managerQRCodeURL = string.Empty,
								}
							}
						}.ToArray(),
					cancellationToken);

			var result =
				new Dictionary<LocalizedDataKey, ProductLocalizedDataInterface>(
					localizedPairs.Length);

			foreach (var pair in localizedPairs)
				if (!result.ContainsKey(pair.Key))
					result.Add(
						pair.Key,
						pair.Value);

			return result;
		}

		private StringEnumConverter _enumConverter =
			new StringEnumConverter();

		async Task<SerializationType> DeserializeData<SerializationType>(string filePath, SerializationType defaultValue, CancellationToken cancellationToken = default)
		{
			var dataFile =
				new FileInfo(
					filePath);

			if (!Directory.Exists(dataFile.DirectoryName))
				Directory.CreateDirectory(
					dataFile.DirectoryName);

			if (!dataFile.Exists)
				await SerializeData(
					filePath,
					defaultValue,
					cancellationToken);

			return
				JsonConvert.DeserializeObject<SerializationType>(
					await File.ReadAllTextAsync(
						dataFile.FullName,
						cancellationToken),
					_enumConverter);
		}

		async Task SerializeData<SerializationType>(string filePath, SerializationType value, CancellationToken cancellationToken = default)
		{
			var dataFile =
				new FileInfo(
					filePath);

			if (!Directory.Exists(dataFile.DirectoryName))
				Directory.CreateDirectory(
					dataFile.DirectoryName);

			await File.WriteAllTextAsync(
				dataFile.FullName,
				JsonConvert.SerializeObject(
					value,
					Formatting.Indented,
					_enumConverter),
				cancellationToken);
		}

		public Task<long> GetLastFileChangesTimestamp(string fileURI) =>
			Task.FromResult(
				UnixDateTimeHelper.currentTimestamp);
	}
}