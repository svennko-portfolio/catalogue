using System;

using UnityEngine;
using UnityEngine.Events;

namespace ICD.Engine.Core.Tools
{
	[Serializable]
	public class FloatAutoProperty : AutoProperty<float>
	{
		[SerializeField]
		private FloatEvent _valueChanged;

		public FloatAutoProperty()
		{
			ValueChanged +=
				value =>
					_valueChanged?.Invoke(
						value);
		}
	}
}
