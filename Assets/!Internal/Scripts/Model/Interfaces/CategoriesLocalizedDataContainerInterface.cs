using System.Collections.Generic;

namespace ICD.Catalogue.Model.Interfaces
{
    public interface CategoriesLocalizedDataContainerInterface : CatalogueItemsLocalizedDataContainerInterface
    {
        public new IEnumerable<CategoryLocalizedDataInterface> all { get; }
        public bool TryGetLocalizedData(uint categoryID, string localeID, out CategoryLocalizedDataInterface localizedData);
        public bool TryGetLocalizedDataForCurrentLocale(uint categoryID, out CategoryLocalizedDataInterface localizedData);
    }

    internal interface WritableCategoriesLocalizedDataContainerInterface : CategoriesLocalizedDataContainerInterface, WritableCatalogueItemsLocalizedDataContainerInterface
    {
        public bool TryAddLocalizedData(uint categoryID, string localeID, CategoryLocalizedDataInterface localizedData, bool owerwriteIfExists = true);
        public new void ClearData();
    }
}