using UnityEngine;
using Sirenix.OdinInspector;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.View
{
	[DisallowMultipleComponent]
	public class PageHeaderController : MonoBehaviour
	{
		[SerializeField]
		[AssetsOnly]
		[Required]
		private PageHeader _headerPrefab;

		[SerializeField]
		[ReadOnly]
		private PageHeader _currentHeader;

		[SerializeField]
		[SceneObjectsOnly]
		private Transform _headerAnchor;

		internal void SetNewHeader(CatalogueItemInterface itemData, bool forwardOrBackward)
		{
			_currentHeader?.Disappear(
				forwardOrBackward);

			_currentHeader =
				Instantiate(
					_headerPrefab,
					_headerAnchor);

			_currentHeader.Appear(
				itemData != null && itemData.localizedData != null && !itemData.localizedData.hideNameInHeader ?
					itemData.localizedData.name :
					null,
				itemData?.localizedData?.logo,
				forwardOrBackward);
		}
	}
}
