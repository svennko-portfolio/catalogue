using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

namespace ICD.Catalogue.View
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Animator))]
	[ExecuteAlways]
	public class PageHeader : MonoBehaviour
	{
		[SerializeField]
		[Required]
		[SceneObjectsOnly]
		private TMP_Text _nameField;

		[SerializeField]
		[Required]
		[SceneObjectsOnly]
		private Image _logoImage;

		[SerializeField]
		[Required]
		private string
			_forwardAnimatorTrigger,
			_backwardAnimatorTrigger;

		private Animator _animator;
		private Animator animator
		{
			get
			{
				if (!_animator)
					_animator = GetComponent<Animator>();
				return _animator;
			}
		}

		private LayoutElement _logoLayoutElement;
		private LayoutElement logoLayoutElement
		{
			get
			{
				if (!_logoLayoutElement)
					_logoLayoutElement = _logoImage.GetComponent<LayoutElement>();
				return _logoLayoutElement;
			}
		}

		internal void Appear(string name, Sprite logo, bool forwardOrBackward)
		{
			_nameField.text = name;
			_logoImage.sprite = logo;

			animator.SetTrigger(
				forwardOrBackward ?
					_forwardAnimatorTrigger :
					_backwardAnimatorTrigger);

		}

		internal void Disappear(bool forwardOrBackward)
		{
			animator.SetTrigger(
				forwardOrBackward ?
					_forwardAnimatorTrigger :
					_backwardAnimatorTrigger);
		}

		void Update()
		{
			_nameField.gameObject.SetActive(
				!string.IsNullOrWhiteSpace(_nameField.text));

			_logoImage.gameObject.SetActive(
				_logoImage.sprite);
			if (_logoImage.sprite)
				logoLayoutElement.preferredWidth =
				logoLayoutElement.minWidth =
				_logoImage.rectTransform.rect.height * _logoImage.sprite.texture.width / _logoImage.sprite.texture.height;
		}
	}
}
