﻿
using UnityEngine;

namespace ICD.Engine.Core.Utility
{
    public class GameObjectMethods : MonoBehaviour
    {
        public void DisactivateSelf() => gameObject.SetActive(false);
        public void DestroySelf() => Destroy(gameObject);
    }
}