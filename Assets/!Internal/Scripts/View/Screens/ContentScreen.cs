using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using ICD.Engine.Core.CustomAttributes;

using ICD.Catalogue.Model.Interfaces;
using ICD.Catalogue.Controller;
using ICD.Catalogue.View.Interfaces;

namespace ICD.Catalogue.View.Screens
{
	public abstract class ContentScreen : PresentationScreen
	{
		[SerializeField]
		[ReadOnly]
		private PageContent _currentContent;
		protected PageContent currentContent => _currentContent;

		[SerializeField]
		[SceneObjectsOnly]
		private Transform _contentRoot;

		[SerializeField]
		[SceneObjectsOnly]
		private Selector _selector;
		protected Selector selector => _selector;

		[SerializeField]
		[Required]
		[RequireInterface(typeof(ContentBuilderInterface))]
		private Object _contentBuilder;
		protected ContentBuilderInterface contentBuilder => _contentBuilder as ContentBuilderInterface;

		[SerializeField]
		private RectTransformEvent _contentChanged;

		private PresentationScreen _presentationScreen;
		protected PresentationScreen presentationScreen
		{
			get
			{
				if (!_presentationScreen)
					_presentationScreen = GetComponent<PresentationScreen>();
				return _presentationScreen;
			}
		}

		public ContentScreen()
		{
			ActivationStateChanged += HandleActivationStateChange;
		}

		public abstract void GoForward(CatalogueItemInterface itemData);

		public abstract void GoBack();

		protected void ConstructPageContent(CatalogueItemInterface itemData)
		{
			_currentContent =
				contentBuilder.Page(
					itemData,
					_contentRoot,
					_selector);
			_currentContent.owner = this;

			_currentContent.Appeared += HandleContentAppearance;

			_contentChanged?.Invoke(
				_currentContent.transform as RectTransform);
		}

		void HandleContentAppearance(PageContent pageContent)
		{
			pageContent.Appeared -= HandleContentAppearance;
			selector.isActive = true;
		}


		void HandleActivationStateChange(bool newState)
		{
			if (newState)
				InputHandler.instance.LeftClickActionPerformed += GoBack;
			else
				InputHandler.instance.LeftClickActionPerformed -= GoBack;
		}
	}
}