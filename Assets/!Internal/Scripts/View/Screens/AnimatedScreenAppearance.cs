using System;
using UnityEngine;

namespace ICD.Catalogue.View.Screens
{
	[RequireComponent(typeof(Animator))]
	public class AnimatedScreenAppearance : ScreenAppearance
	{
		public override event Action Appeared;

		public void SetScreenAppeared() =>
			Appeared?.Invoke();
	}
}