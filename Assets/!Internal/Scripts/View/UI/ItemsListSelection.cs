using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

using ICD.Catalogue.Controller;
using ICD.Catalogue.View.Interfaces;
using UnityEngine.UI;

namespace ICD.Catalogue.View
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Pages/Selection/List Items Selection", fileName = "List Items Selection")]
	public class ItemsListSelection : SelectionBehaviour
	{
		private IReadOnlyList<SelectableItemInterface> _items;
		internal IReadOnlyList<SelectableItemInterface> items
		{
			set => _items = value;
		}

		public override SelectableItemInterface Process(RectTransform selectionHandler, ScrollRect scroll)
		{
			if (!selectionHandler)
				return null;

			var relativeRectangle =
				GetRelativeRectangle(
					selectionHandler,
					scroll);
			var handlerNormalizedPosition =
				GetHandlerNormalizedPosition(
					selectionHandler,
					relativeRectangle);

			if (!scroll)
			{
				SnapToNormalizedPosition(
					selectionHandler,
					relativeRectangle,
					0);
				return null;
			}

			var inputValue = InputHandler.instance.verticalMovementActionValue;
			if (inputValue == 0)
			{
				var closestItem =
					FindClosestItem(
						selectionHandler);

				if (closestItem == null)
				{
					SnapToNormalizedPosition(
						selectionHandler,
						relativeRectangle,
						0);
					return null;
				}

				var position = selectionHandler.position;
				position.y =
					Mathf.Lerp(
						position.y,
						closestItem.position.y,
						snapIntencity);
				selectionHandler.position = position;

				scroll.verticalNormalizedPosition =
					1 -
					GetHandlerNormalizedPosition(
						selectionHandler,
						relativeRectangle);

				return closestItem;
			}
			else
			{
				if (handlerNormalizedPosition < 0 && inputValue > 0)
					inputValue /= Mathf.Pow(1 - handlerNormalizedPosition, bordersDrag);
				else
				if (handlerNormalizedPosition > 1 && inputValue < 0)
					inputValue /= Mathf.Pow(handlerNormalizedPosition, bordersDrag);

				var position = selectionHandler.localPosition;
				position.y += inputValue * scroll.scrollSensitivity * sensitivityMultiplier;
				selectionHandler.localPosition = position;

				if (scroll.content && scroll.viewport &&
					scroll.content.rect.height > scroll.viewport.rect.height)
					scroll.verticalNormalizedPosition =
						1 -
						GetHandlerNormalizedPosition(
							selectionHandler,
							relativeRectangle);

				return null;
			}
		}

		SelectableItemInterface FindClosestItem(RectTransform selectionHandler)
		{
			if (_items == null || _items.Count == 0)
				return null;

			SelectableItemInterface closestItem = null;
			var minimalDistance = float.PositiveInfinity;

			foreach (var item in _items)
			{
				var distance =
					Mathf.Abs(
						selectionHandler.position.y -
						item.position.y);
				if (distance < minimalDistance)
				{
					closestItem = item;
					minimalDistance = distance;
				}
			}

			return closestItem;
		}
	}
}