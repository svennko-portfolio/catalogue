﻿using System;

using UnityEngine;
using UnityEngine.Localization;
using Newtonsoft.Json;

namespace ICD.Catalogue.Model
{
	[Serializable]
	public struct LocalizedDataKey : IEquatable<LocalizedDataKey>
	{
		[SerializeField]
		private uint _catalogueItemID;

		[JsonProperty("id", Required = Required.Always)]
		public uint catalogueItemID
		{
			get => _catalogueItemID;
			set => _catalogueItemID = value;
		}

		[SerializeField]
		private string _localeID;

		[JsonProperty("locale_id", Required = Required.Always)]
		public string localeID
		{
			get => _localeID;
			set => _localeID = value;
		}

		public LocalizedDataKey(uint catalogueItemID, string localeID)
		{
			_catalogueItemID = catalogueItemID;
			_localeID = localeID;
		}

		public static LocalizedDataKey GetLocalizedDataKeyForCurrentLocale(uint catalogueItemID) =>
			new LocalizedDataKey(
				catalogueItemID,
				LocalizationHelper.currentLocaleID);

		public static bool operator ==(LocalizedDataKey key1, LocalizedDataKey key2) =>
			key1._catalogueItemID == key2._catalogueItemID &&
			key1._localeID == key2._localeID;

		public static bool operator !=(LocalizedDataKey key1, LocalizedDataKey key2) =>
			key1._catalogueItemID != key2._catalogueItemID ||
			key1._localeID != key2._localeID;


		public bool Equals(LocalizedDataKey other) =>
			other._catalogueItemID == _catalogueItemID &&
			other._localeID == _localeID;

		public override bool Equals(object otherObject) =>
			otherObject != null && otherObject is LocalizedDataKey &&
			((LocalizedDataKey)otherObject).Equals(
				this);

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = catalogueItemID.GetHashCode();
				if (!string.IsNullOrWhiteSpace(localeID))
					hashCode ^= localeID.GetHashCode();
				return hashCode;
			}
		}

		public override string ToString() =>
			$"{nameof(catalogueItemID)}: {catalogueItemID}; {nameof(localeID)}: {localeID}";
	}
}