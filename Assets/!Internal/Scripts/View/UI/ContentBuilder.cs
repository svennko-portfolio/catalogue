using UnityEngine;
using Sirenix.OdinInspector;

using ICD.Catalogue.View.Interfaces;
using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.View
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Pages/Content Builder", fileName = "Content Builder")]
	public class ContentBuilder : ScriptableObject, ContentBuilderInterface
	{
		[SerializeField]
		[Required]
		[AssetsOnly]
		private ListContent
			_rootCategoriesPageContentPrefab,
			_subcategoriesPageContentPrefab,
			_productsListPageContentPrefab;

		[SerializeField]
		[Required]
		[AssetsOnly]
		private ProductContent _productPageContentPrefab;

		[SerializeField]
		private ItemsListSelection _itemsListSelection;

		[SerializeField]
		private SelectionBehaviour _scrollSelection;


		public PageContent Page(CatalogueItemInterface pageData, Transform contentRoot, Selector selector)
		{
			PageContent prefab;
			SelectionBehaviour selectionBehaviour;
			switch (pageData)
			{
				case CategoryInterface category:
					if (category.mainData.contentType == CategoryMainDataInterface.ContentType.categories)
						prefab = _subcategoriesPageContentPrefab;
					else
						prefab = _productsListPageContentPrefab;
					selectionBehaviour = _itemsListSelection;
					break;
				case ProductInterface product:
					prefab = _productPageContentPrefab;
					selectionBehaviour = _scrollSelection;
					break;
				default:
					prefab = _rootCategoriesPageContentPrefab;
					selectionBehaviour = _itemsListSelection;
					break;
			}

			var content =
				Instantiate(
					prefab,
					contentRoot);
			content.Construct(
				pageData);

			var listSelection = selectionBehaviour as ItemsListSelection;
			var listContent = content as ListContent;
			if (listSelection != null && listContent != null)
				listSelection.items = listContent.items;

			if (selector)
				selector.selectionBehaviour = selectionBehaviour;

			return content;
		}
	}
}
