namespace ICD.Catalogue.Model.Interfaces
{
	public interface CategoryInterface : CatalogueItemInterface
	{
		public new CategoryMainDataInterface mainData { get; }
		public new CategoryLocalizedDataInterface localizedData { get; }
	}
}