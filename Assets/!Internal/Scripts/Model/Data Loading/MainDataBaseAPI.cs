using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Localization;
using Sirenix.OdinInspector;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using ICD.Catalogue.Model.Interfaces;
using ICD.Catalogue.Model.DataLoading.Interfaces;


namespace ICD.Catalogue.Model.DataLoading
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Data/API/Main Data Base API", fileName = "Main Data Base API")]
	internal class MainDataBaseAPI : ScriptableObject, DataBaseAPI
	{
		[SerializeField]
		[Required]
		private DataBaseAPIHost _host;

		[SerializeField]
		private ushort _port;

		[SerializeField]
		[ReadOnly]
		private AuthorizationData _authorizationData;

		public bool isLoggedIn =>
			!_authorizationData.isEmpty;

		[SerializeField]
		private string
			_logInRequestPath = "/login",

			_allCategorisedRequestPath = "/categories",
			_categoryRequestPath = "/categories/{ID}",
			_categoryLocalizedDataRequestPath = "/categories/{ID}/localized",

			_allProductsRequestPath = "/products",
			_productRequestPath = "/products/{ID}",
			_productLocalizedDataRequestPath = "/products/{ID}/localized",

			_localeParameterName = "locale";

		[SerializeField]
		[Required]
		private string _itemIDPlaceHolder = "{ID}";

		private StringEnumConverter _enumConverter =
			new StringEnumConverter();

		public MainDataBaseAPI()
		{
			Application.quitting +=
				() => LogOut();
		}

		public Task<bool> LogIn(string username, string password, CancellationToken cancellationToken = default) =>
			LogIn(
				new DataBaseCredentials()
				{
					username = username,
					password = password,
				},
				cancellationToken);

		public async Task<bool> LogIn(DataBaseCredentials credentials, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(cancellationToken))
				return false;

			using (var client = new HttpClient())
			{
				try
				{
					var requestURLBuilder = _host.APIURLBuilder;
					requestURLBuilder.Path += _logInRequestPath;

					client.DefaultRequestHeaders.Clear();
					client.DefaultRequestHeaders.Accept.Add(
						new MediaTypeWithQualityHeaderValue(
							"application/json"));

					var response =
						await client.SendAsync(
							new HttpRequestMessage(
								HttpMethod.Post,
								requestURLBuilder.Uri)
							{
								Content =
									new StringContent(
										JsonConvert.SerializeObject(
											credentials),
										Encoding.UTF8,
										"application/json")
							},
							cancellationToken);

					var isSuccessful = response.StatusCode == HttpStatusCode.OK;
					var responseString = await response.Content.ReadAsStringAsync();

					if (isSuccessful)
					{
						_authorizationData =
							JsonConvert.DeserializeObject<AuthorizationData>(
								responseString);
					}
					else
					{
						Debug.LogError(
							$"Failed to log in:\n" +
							responseString);
					}

					response.Dispose();
					return isSuccessful;
				}
				catch (Exception exception)
				{
					Debug.LogError(
						$"Failed to login:\n" +
						exception + ":\n" +
						exception.StackTrace);
					return false;
				}
			}
		}

		public Task<bool> LogOut(CancellationToken cancellationToken = default)
		{
			_authorizationData.Clear();
			return
				Task.FromResult(
					true);
		}

		public async Task<IEnumerable<CategoryMainDataInterface>> GetAllCategories(CancellationToken cancellationToken = default)
		{
			var requestURLBuilder = _host.APIURLBuilder;
			requestURLBuilder.Path += _allCategorisedRequestPath;

			return
				await RequestData<IEnumerable<CategoryMainData>>(
					requestURLBuilder.Uri,
					"Faild to load categories main data",
					cancellationToken);
		}

		public async Task<CategoryMainDataInterface> GetCategoriy(uint categoryID, CancellationToken cancellationToken = default)
		{
			var requestURLBuilder = _host.APIURLBuilder;
			requestURLBuilder.Path =
				_categoryRequestPath.Replace(
					_itemIDPlaceHolder,
					categoryID.ToString());

			return
				await RequestData<CategoryMainData>(
					requestURLBuilder.Uri,
					$"Faild to load category \"{categoryID}\" main data",
					cancellationToken);
		}

		public async Task<IEnumerable<ProductMainDataInterface>> GetAllProducts(CancellationToken cancellationToken = default)
		{
			var requestURLBuilder = _host.APIURLBuilder;
			requestURLBuilder.Path += _allProductsRequestPath;

			return
				await RequestData<IEnumerable<ProductMainData>>(
					requestURLBuilder.Uri,
					"Faild to load products main data",
					cancellationToken);
		}

		public async Task<ProductMainDataInterface> GetProduct(uint productID, CancellationToken cancellationToken = default)
		{
			var requestURLBuilder = _host.APIURLBuilder;
			requestURLBuilder.Path +=
				_productRequestPath.Replace(
					_itemIDPlaceHolder,
					productID.ToString());

			return
				await RequestData<ProductMainData>(
					requestURLBuilder.Uri,
					$"Faild to load product \"{productID}\" main data",
					cancellationToken);
		}

		public async Task<IReadOnlyDictionary<string, CategoryLocalizedDataInterface>> GetCategoryLocalizedData(uint categoryID, CancellationToken cancellationToken = default)
		{
			var result =
				new Dictionary<string, CategoryLocalizedDataInterface>(
					LocalizationHelper.availableLocalesLocalesCount);

			foreach (var localeID in LocalizationHelper.availableLocales)
			{
				if (CheckCancellation(
						cancellationToken,
						nameof(GetCategoryLocalizedData)))
					return null;

				var localizedData =
					await GetCategoryLocalizedData(
						categoryID,
						localeID,
						cancellationToken);

				if (localizedData != null && !result.ContainsKey(localeID))
					result.Add(
						localeID,
						localizedData);
			}

			return result;
		}

		public async Task<CategoryLocalizedDataInterface> GetCategoryLocalizedData(uint categoryID, string localeID, CancellationToken cancellationToken = default)
		{
			var requestURLBuilder = _host.APIURLBuilder;
			requestURLBuilder.Path +=
				_categoryLocalizedDataRequestPath.Replace(
					_itemIDPlaceHolder,
					categoryID.ToString());
			requestURLBuilder.Query = $"{_localeParameterName}={localeID}";

			return
				await RequestData<CategoryLocalizedData>(
					requestURLBuilder.Uri,
					$"Faild to load localized data of category \"{categoryID}\" for locale \"{localeID}\"",
					cancellationToken);
		}

		public async Task<IReadOnlyDictionary<string, ProductLocalizedDataInterface>> GetProductLocalizedData(uint productID, CancellationToken cancellationToken = default)
		{
			var result =
				new Dictionary<string, ProductLocalizedDataInterface>(
					LocalizationHelper.availableLocalesLocalesCount);

			foreach (var localeID in LocalizationHelper.availableLocales)
			{
				if (CheckCancellation(
						cancellationToken,
						nameof(GetProductLocalizedData)))
					return null;

				var localizedData =
					await GetProductLocalizedData(
						productID,
						localeID,
						cancellationToken);

				if (localizedData != null && !result.ContainsKey(localeID))
					result.Add(
						localeID,
						localizedData);
			}

			return result;
		}

		public async Task<ProductLocalizedDataInterface> GetProductLocalizedData(uint productID, string localeID, CancellationToken cancellationToken = default)
		{
			var requestURLBuilder = _host.APIURLBuilder;
			requestURLBuilder.Path +=
				_productLocalizedDataRequestPath.Replace(
					_itemIDPlaceHolder,
					productID.ToString());
			requestURLBuilder.Query = $"{_localeParameterName}={localeID}";

			return
				await RequestData<ProductLocalizedData>(
					requestURLBuilder.Uri,
					$"Faild to load localized data of product \"{productID}\" for locale \"{localeID}\"",
					cancellationToken);
		}

		async Task<DataType> RequestData<DataType>(Uri requestURL, string errorTitle, CancellationToken cancellationToken = default)
		{
			try
			{
				using (var client = new HttpClient())
				{
					client.DefaultRequestHeaders.Clear();
					client.DefaultRequestHeaders.Accept.Add(
						new MediaTypeWithQualityHeaderValue(
							"application/json"));
					client.DefaultRequestHeaders.Authorization =
						new AuthenticationHeaderValue(
							_authorizationData.scheme,
							_authorizationData.accessToken);

					var response =
						await client.SendAsync(
							new HttpRequestMessage(
								HttpMethod.Get,
								requestURL),
							cancellationToken);
					var responseString = await response.Content.ReadAsStringAsync();

					DataType result = default;
					if (response.StatusCode == HttpStatusCode.OK)
						result =
							JsonConvert.DeserializeObject<DataType>(
								responseString,
								_enumConverter);
					else
						Debug.LogError(
							errorTitle + ":\n" +
							responseString);

					response.Dispose();
					return result;
				}
			}
			catch (Exception exception)
			{
				Debug.LogError(
					errorTitle + ":\n" +
					exception + ":\n" +
					exception.StackTrace);
				return default;
			}
		}

		public Task<long> GetLastFileChangesTimestamp(string fileURI) =>
			Task.FromResult(
				UnixDateTimeHelper.currentTimestamp);

		bool CheckCancellation(CancellationToken cancellationToken, string operationName = null)
		{
			if (cancellationToken.IsCancellationRequested)
			{
				Debug.Log(
					$"Operation \"{operationName}\" cancelled");
				return true;
			}
			return false;
		}
	}

	[Serializable]
	public struct AuthorizationData
	{
		[SerializeField]
		private string
			_scheme,
			_accessToken;

		[JsonProperty("token_type")]
		public string scheme
		{
			get => _scheme;
			set => _scheme = value;
		}

		[JsonProperty("access_token")]
		public string accessToken
		{
			get => _accessToken;
			set => _accessToken = value;
		}

		public bool isEmpty =>
			string.IsNullOrWhiteSpace(
				_scheme) ||
			string.IsNullOrWhiteSpace(
				_accessToken);

		[ContextMenu("Clear")]
		public void Clear()
		{
			_scheme = null;
			_accessToken = null;
		}
	}
}