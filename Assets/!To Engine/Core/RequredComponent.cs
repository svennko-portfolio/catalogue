using System;

using UnityEngine;

namespace ICD.Engine.Core.Utility
{
    public class LazzyComponentGetter<ComponentType>
        where ComponentType : Component
    {
        private ComponentType _component;
        public ComponentType component
		{
			get
			{
				if (!_component)
					_component = _owner.GetComponent<ComponentType>();
				return _component;
			}
		}

        private Component _owner;
        public LazzyComponentGetter(Component owner)
		{
			if (!owner)
				throw new ArgumentNullException(
					nameof(owner));
			_owner = owner;
		}
	}
}
