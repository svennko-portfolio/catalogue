
using System;

using UnityEngine;

namespace ICD.Engine.Core.CustomAttributes
{
    public class RequireInterfaceAttribute : PropertyAttribute
    {
        public Type requiredType { get; }

        public RequireInterfaceAttribute(Type requiredType) =>
            this.requiredType = requiredType;
    }
}
