using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using ICD.Engine.Core.Singletons;

namespace ICD.Catalogue.Controller
{
    [DisallowMultipleComponent]
    public class LocalizationController : Singleton<LocalizationController>
    {
        [SerializeField]
        private StringEvent _localChanged;

        public override bool dontDestroyOnLoad => false;

        public void SetLocale(string localeID) =>
            LocalizationHelper.SetCurrentLocale(
                localeID);

        void HandleLocaleChange(Locale newLocale)
        {
            _localChanged?.Invoke(
                newLocale.Identifier.Code);
        }

        void Awake()
        {
			LocalizationSettings.SelectedLocaleChanged += HandleLocaleChange;
        }

		void OnDestroy()
        {
            LocalizationSettings.SelectedLocaleChanged -= HandleLocaleChange;
        }
    }
}