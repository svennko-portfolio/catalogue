namespace ICD.Catalogue.Model.Interfaces
{
	public interface CategoryMainDataInterface : CatalogueItemMainDataInterface
	{
		public enum ContentType
		{
			categories,
			products
		}

		public ContentType contentType { get; }
	}
}