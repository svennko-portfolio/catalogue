using System;

namespace UnityEngine.Events
{
	public abstract class UnityEventsComposition<EventValueType>
	{
		public abstract void Invoke(EventValueType value);
	}

	[Serializable]
	public class BooleanEventsComposition : UnityEventsComposition<bool>
	{
		[SerializeField]
		private BooleanEvent
			_invoked,
			_invokedInverted;

		[SerializeField]
		private UnityEvent
			_trueValueInvoked,
			_falseValueInvoked;

		public override void Invoke(bool value)
		{
			_invoked?.Invoke(value);
			_invokedInverted?.Invoke(!value);

			if (value)
				_trueValueInvoked?.Invoke();
			else
				_falseValueInvoked?.Invoke();
		}
	}
}