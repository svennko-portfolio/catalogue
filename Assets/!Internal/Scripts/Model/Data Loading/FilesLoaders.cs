﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

using ICD.Catalogue.Model.DataLoading.Interfaces;

namespace ICD.Catalogue.Model
{
	internal abstract class FilesLoader : FilesLoaderInterface
	{
		protected readonly char[] _charsBeingRemovedFromURI =
			new char[] { '/' };

		public abstract Task<FileInfo> LoadFile(Uri downLoadURL, DirectoryInfo cacheDirectory, CancellationToken cancellationToken = default);

		protected async Task<FileInfo> DownloadFile(Uri downLoadURL, string localFilePath, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(
				cancellationToken,
				nameof(LoadFile)))
				return null;

			using (var client = new WebClient())
			{
				if (File.Exists(localFilePath))
					File.Delete(
						localFilePath);

				var file =
					new FileInfo(
						localFilePath);

				if (!Directory.Exists(file.DirectoryName))
					Directory.CreateDirectory(
						file.DirectoryName);

				var downloadingTask =
					client.DownloadFileTaskAsync(
						downLoadURL,
						file.FullName);

				while (!downloadingTask.IsCompleted)
				{
					if (cancellationToken.IsCancellationRequested)
					{
						client.CancelAsync();
						return null;
					}

					await Task.Yield();
				}

				return
					file;
			}
		}

		protected bool CheckCancellation(CancellationToken cancellationToken, string operationName = null)
		{
			if (cancellationToken.IsCancellationRequested)
			{
				Debug.Log(
					$"Operation {operationName} cancelled");
				return true;
			}
			return false;
		}
	}

	internal class CacheOnlyLoader : FilesLoader
	{
		public override Task<FileInfo> LoadFile(Uri downLoadURL, DirectoryInfo cacheDirectory, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(cancellationToken) ||
				downLoadURL == null || string.IsNullOrWhiteSpace(downLoadURL.OriginalString))
				return null;

			return
				Task.FromResult(
					new FileInfo(
						Path.Combine(
							cacheDirectory.FullName,
							downLoadURL.LocalPath)));
		}
	}

	internal class CachePreferedLoader : FilesLoader
	{
		public override async Task<FileInfo> LoadFile(Uri downLoadURL, DirectoryInfo cacheDirectory, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(cancellationToken) ||
				downLoadURL == null || string.IsNullOrWhiteSpace(downLoadURL.OriginalString))
				return null;

			var cachedFile =
				new FileInfo(
					Path.Combine(
						cacheDirectory.FullName,
						downLoadURL.LocalPath.TrimStart(
							_charsBeingRemovedFromURI)));

			if (!cachedFile.Exists)
				cachedFile =
					await DownloadFile(
						downLoadURL,
						cachedFile.FullName,
						cancellationToken);

			return cachedFile;
		}
	}

	internal class ChangesTimeBasedLoader : FilesLoader
	{
		private DataBaseAPI dataBaseAPI { get; }

		private FilesLoader
			_cachePreferedLoader = new CachePreferedLoader(),
			_downloadingPreferedLoader = new DownloadingPreferdLoader();

		public ChangesTimeBasedLoader(DataBaseAPI dataBaseAPI)
		{
			if (dataBaseAPI == null)
				throw new ArgumentNullException(
					nameof(dataBaseAPI));

			this.dataBaseAPI = dataBaseAPI;
		}

		public override async Task<FileInfo> LoadFile(Uri downLoadURL, DirectoryInfo cacheDirectory, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(cancellationToken) ||
				downLoadURL == null || string.IsNullOrWhiteSpace(downLoadURL.OriginalString))
				return null;

			var cachedFile =
				new FileInfo(
					Path.Combine(
						cacheDirectory.FullName,
						downLoadURL.LocalPath.TrimStart(
							_charsBeingRemovedFromURI)));

			var cahedFileWriteTimestamp =
				cachedFile.Exists ?
					File.GetLastWriteTimeUtc(
						cachedFile.FullName).ToUnixTimestamp() :
					long.MinValue;

			var remoteFileCahngesTimestamp =
				dataBaseAPI != null ?
					await dataBaseAPI.GetLastFileChangesTimestamp(
						downLoadURL.AbsolutePath) :
					long.MinValue;

			return
				remoteFileCahngesTimestamp > cahedFileWriteTimestamp ?
					await _downloadingPreferedLoader.LoadFile(
						downLoadURL,
						cacheDirectory,
						cancellationToken) :
					await _cachePreferedLoader.LoadFile(
						downLoadURL,
						cacheDirectory,
						cancellationToken);
		}
	}

	internal class DownloadingPreferdLoader : FilesLoader
	{
		public override async Task<FileInfo> LoadFile(Uri downLoadURL, DirectoryInfo cacheDirectory, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(cancellationToken) ||
				downLoadURL == null || string.IsNullOrWhiteSpace(downLoadURL.OriginalString))
				return null;

			var cachedFile =
				new FileInfo(
					Path.Combine(
						cacheDirectory.FullName,
						downLoadURL.LocalPath.TrimStart(
							_charsBeingRemovedFromURI)));
			var reservedCachedFile =
				new FileInfo(
					cachedFile.FullName + " (RESERVED)" + cachedFile.Extension);
			if (reservedCachedFile.Exists)
				File.Delete(
					reservedCachedFile.FullName);

			if (cachedFile.Exists)
				File.Move(
					cachedFile.FullName,
					reservedCachedFile.FullName);

			try
			{
				await DownloadFile(
					downLoadURL,
					cachedFile.FullName,
					cancellationToken);

				if (cancellationToken.IsCancellationRequested)
					throw new OperationCanceledException(
						cancellationToken);

				if (reservedCachedFile.Exists)
					File.Delete(
						reservedCachedFile.FullName);
			}
			catch (Exception exception)
			{
				Debug.LogError(
					$"Failed to load file from: {downLoadURL}:\n" +
					exception);

				if (reservedCachedFile.Exists)
					File.Move(
						reservedCachedFile.FullName,
						cachedFile.FullName);
			}

			return cachedFile;
		}
	}

	internal class DownloadingOnlyLoader : FilesLoader
	{
		public override async Task<FileInfo> LoadFile(Uri downLoadURL, DirectoryInfo cacheDirectory, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(cancellationToken) ||
				downLoadURL == null || string.IsNullOrWhiteSpace(downLoadURL.OriginalString))
				return null;

			var cachedFile =
				new FileInfo(
					Path.Combine(
						cacheDirectory.FullName,
						downLoadURL.LocalPath.TrimStart(
							_charsBeingRemovedFromURI)));

			await DownloadFile(
				downLoadURL,
				cachedFile.FullName,
				cancellationToken);

			return cachedFile;
		}
	}

	internal static class UnixDateTimeHelper
	{
		public static readonly DateTime referenceDate =
			new DateTime(
				1970,
				1,
				1,
				0,
				0,
				0,
				DateTimeKind.Utc);

		public static long currentTimestamp =>
			DateTime.Now.ToUnixTimestamp();

		public static long ToUnixTimestamp(this DateTime dateTime) =>
			(long)
				(dateTime -
				referenceDate).TotalSeconds;

		public static DateTime FromUnixTimestamp(this long timestamp) =>
			referenceDate.AddSeconds(
				timestamp);
	}
}