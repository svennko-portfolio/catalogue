using UnityEngine;
using UnityEngine.UI;

using ICD.Catalogue.Controller;
using ICD.Catalogue.View.Interfaces;

namespace ICD.Catalogue.View
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Pages/Selection/Scroll Selection", fileName = "Scroll Selection")]
	public class ScrollSelection : SelectionBehaviour
	{
		public override SelectableItemInterface Process(RectTransform selectionHandler, ScrollRect scroll)
		{
			if (!selectionHandler)
				return null;

			var relativeRectangle =
				GetRelativeRectangle(
					selectionHandler,
					scroll);
			var handlerNormalizedPosition =
				GetHandlerNormalizedPosition(
					selectionHandler,
					relativeRectangle);

			if (!scroll)
			{
				SnapToNormalizedPosition(
					selectionHandler,
					relativeRectangle,
					0);
				return null;
			}

			var inputValue = InputHandler.instance.verticalMovementActionValue;
			if (inputValue != 0)
			{
				if (handlerNormalizedPosition < 0 && inputValue > 0)
					inputValue /= Mathf.Pow(1 - handlerNormalizedPosition, bordersDrag);
				else
				if (handlerNormalizedPosition > 1 && inputValue < 0)
					inputValue /= Mathf.Pow(handlerNormalizedPosition, bordersDrag);

				var position = selectionHandler.localPosition;
				position.y += inputValue * scroll.scrollSensitivity * sensitivityMultiplier;
				selectionHandler.localPosition = position;

				if (scroll.content && scroll.viewport &&
					scroll.content.rect.height > scroll.viewport.rect.height)
					scroll.verticalNormalizedPosition =
						1 -
						GetHandlerNormalizedPosition(
							selectionHandler,
							relativeRectangle);

			}

			return null;
		}
	}
}