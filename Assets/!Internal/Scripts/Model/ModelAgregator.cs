using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using Sirenix.OdinInspector;
using ICD.Engine.Core.CustomAttributes;

using ICD.Catalogue.Model.Interfaces;
using ICD.Catalogue.Model.DataLoading.Interfaces;
using FilesLoadingPolicy = ICD.Catalogue.Model.DataLoading.Interfaces.DataLoaderInterface.FilesLoadingPolicy;

namespace ICD.Catalogue.Model
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Data/Model", fileName = "Model")]
	public class ModelAgregator : ScriptableObject
	{
		private static ModelAgregator _instance;
		private static object _instanceLocker = new object();
		public static ModelAgregator instance
		{
			get
			{
				lock (_instanceLocker)
				{
					if (!_instance)
						_instance = Resources.Load<ModelAgregator>(nameof(ModelAgregator));

					if (!_instance)
					{
						var models = Resources.LoadAll<ModelAgregator>(string.Empty);

						if (models != null && models.Length > 0)
							_instance = models[0];
					}

					if (!_instance)
					{
						var models = Resources.FindObjectsOfTypeAll<ModelAgregator>();

						if (models != null && models.Length > 0)
							_instance = models[0];
					}
				}

				return _instance;
			}
		}

		[SerializeField]
		[Required]
		[AssetsOnly]
		[RequireInterface(typeof(DataLoaderInterface))]
		private Object _dataLoader;
		private DataLoaderInterface dataLoader => _dataLoader as DataLoaderInterface;

		[SerializeField]
		[Required]
		[AssetsOnly]
		[RequireInterface(typeof(WritableCategoriesLocalizedDataContainerInterface))]
		private Object _categoriesLocalizedDataContainter;
		private WritableCategoriesLocalizedDataContainerInterface writableCategoriesLocalizedDataContainter => _categoriesLocalizedDataContainter as WritableCategoriesLocalizedDataContainerInterface;
		public CategoriesLocalizedDataContainerInterface categoriesLocalizedDataContainter => writableCategoriesLocalizedDataContainter;

		[SerializeField]
		[Required]
		[AssetsOnly]
		[RequireInterface(typeof(WritableProductsLocalizedDataContainerInterface))]
		private Object _productsLocalizedDataContainter;
		private WritableProductsLocalizedDataContainerInterface writableProductsLocalizedDataContainter => _productsLocalizedDataContainter as WritableProductsLocalizedDataContainerInterface;
		public ProductsLocalizedDataContainerInterface productsLocalizedDataContainter => writableProductsLocalizedDataContainter;

		[SerializeField]
		[Required]
		[AssetsOnly]
		[RequireInterface(typeof(WritableCatalogueItemsContainerInterface))]
		private Object _catalogueItemsContainer;
		private WritableCatalogueItemsContainerInterface writableCatalogueItemsContainer => _catalogueItemsContainer as WritableCatalogueItemsContainerInterface;
		public CatalogueItemsContainerInterface catalogueItemsContainer => writableCatalogueItemsContainer;

		[SerializeField]
		[ReadOnly]
		private bool _isLoadingNow;
		public bool isLoadingNow => _isLoadingNow;

		[ContextMenu("Load Data")]
		internal async void LoadData() =>
			await LoadData(
				CancellationToken.None);

		public CatalogueItemInterface lastRequiredItem { get; set; }

		internal async Task LoadData(FilesLoadingPolicy filesLoadingPolicy, CancellationToken cancellationToken = default)
		{
			if (_isLoadingNow)
			{
				Debug.LogError(
					$"Data is loading now");
			}

			_isLoadingNow = true;

			ClearData();

			await dataLoader.LoadMainData(
				writableCatalogueItemsContainer,
				writableCategoriesLocalizedDataContainter,
				writableProductsLocalizedDataContainter,
				cancellationToken);
			if (cancellationToken.IsCancellationRequested)
			{
				Debug.Log("Main data loading cancelled");
				_isLoadingNow = false;
				return;
			}
			else
				Debug.Log("Main data loaded");

			await dataLoader.LoadMediaData(
				catalogueItemsContainer,
				writableCategoriesLocalizedDataContainter,
				writableProductsLocalizedDataContainter,
				filesLoadingPolicy,
				cancellationToken);
			if (cancellationToken.IsCancellationRequested)
			{
				Debug.Log("Media data loading cancelled");
				_isLoadingNow = false;
				return;
			}
			else
				Debug.Log("Media data loaded");

			_isLoadingNow = false;
		}

		internal async Task LoadData(CancellationToken cancellationToken = default)
		{

			ClearData();

			await dataLoader.LoadMainData(
				writableCatalogueItemsContainer,
				writableCategoriesLocalizedDataContainter,
				writableProductsLocalizedDataContainter,
				cancellationToken);
			Debug.Log("Main data loaded");

			await dataLoader.LoadMediaData(
				catalogueItemsContainer,
				writableCategoriesLocalizedDataContainter,
				writableProductsLocalizedDataContainter,
				dataLoader.defaultFilesLoadingPolicy,
				cancellationToken);
			Debug.Log("Media data loaded");
		}

		internal void ClearData()
		{
			writableCategoriesLocalizedDataContainter.ClearData();
			writableProductsLocalizedDataContainter.ClearData();
			writableCatalogueItemsContainer.ClearData();
			System.GC.Collect();
		}
	}
}