using UnityEngine;
using UnityEngine.UI;
using ICD.Catalogue.View.Interfaces;

namespace ICD.Catalogue.View
{
	public abstract class SelectionBehaviour : ScriptableObject, SelectionBehaviourInterface
	{
		[SerializeField]
		[Min(0)]
		private float _sensitivityMultiplier = 1;
		protected float sensitivityMultiplier => _sensitivityMultiplier;

		[SerializeField]
		[Min(0)]
		private float _snapIntencity;
		protected float snapIntencity => _snapIntencity;

		[SerializeField]
		[Min(0)]
		private float _snapTreshold = 1;
		protected float snapTreshold => _snapTreshold;

		[SerializeField]
		[Min(0)]
		private float _bordersDrag = 1;
		protected float bordersDrag => _bordersDrag;

		protected float GetHandlerNormalizedPosition(RectTransform selectionHandler, Rect relativeRectangle)
		{
			if (!selectionHandler?.parent)
				return 0f;

			var height = selectionHandler.rect.height;
			var handlerTopPosition = selectionHandler.localPosition.y + height * (1 - selectionHandler.pivot.y);

			return
				((selectionHandler.parent as RectTransform).rect.height / 2 - handlerTopPosition) /
				(relativeRectangle.height - height);
		}

		protected void SetHandlerNormalizedPosition(RectTransform selectionHandler, Rect relativeRectangle, float value)
		{
			var localPosition = selectionHandler.localPosition;
			localPosition.y =
				(selectionHandler.parent as RectTransform).rect.height / 2 -
				value * (relativeRectangle.height - selectionHandler.rect.height) -
				selectionHandler.rect.height * (1 - selectionHandler.pivot.y);
			selectionHandler.localPosition = localPosition;
		}

		protected void SnapToNormalizedPosition(RectTransform selectionHandler, Rect relativeRectangle, float requiredNormalizedPosition)
		{
			var handlerNormalizedPosition =
				GetHandlerNormalizedPosition(
					selectionHandler,
					relativeRectangle);

			SetHandlerNormalizedPosition(
				selectionHandler,
				relativeRectangle,
				Mathf.Lerp(
					handlerNormalizedPosition,
					requiredNormalizedPosition,
					_snapIntencity));
		}

		protected Rect GetRelativeRectangle(RectTransform selectionHandlerhandler, ScrollRect scroll)
		{
			if (!selectionHandlerhandler)
				return Rect.zero;

			return
				!scroll?.content || !scroll?.viewport ||
				scroll.content.rect.height > scroll.viewport.rect.height ?
					(selectionHandlerhandler.parent as RectTransform).rect :
					scroll.content.rect;
		}

		public abstract SelectableItemInterface Process(RectTransform selectionHandler, ScrollRect scroll);

		public void ProcessInaction(RectTransform selectionHandler, ScrollRect scroll) =>
			SnapToNormalizedPosition(
				selectionHandler,
				GetRelativeRectangle(
					selectionHandler,
					scroll),
				0);
	}
}