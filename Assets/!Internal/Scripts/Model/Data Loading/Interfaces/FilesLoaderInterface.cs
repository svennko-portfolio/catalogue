﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace ICD.Catalogue.Model.DataLoading.Interfaces
{
	internal interface FilesLoaderInterface
	{
		public Task<FileInfo> LoadFile(Uri downLoadURL, DirectoryInfo cacheDirectory, CancellationToken cancellationToken = default);
	}
}