﻿using System;

using UnityEngine;
using ICD.Engine.Core.Utility;

using ICD.Catalogue.Model.DataLoading.Interfaces;

namespace ICD.Catalogue.Model.DataLoading
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Data/API/Refreshable Data Base Host", fileName = "Refreshable Data Base Host")]
	public class RefreshableDataBaseAPIHost : DataBaseAPIHost
	{
		public override UriBuilder APIURLBuilder
		{
			get
			{
				RefreshFromSettings();

				return base.APIURLBuilder;
			}
		}

		[ContextMenu("Refresh From Settings")]
		internal void RefreshFromSettings()
		{
			INISetting.ReloadINI();

			_scheme =
				INISetting.GetValueWithAdd(
					DataBaseAPI.DATA_BASE_SETTINGS_GROUP_NAME,
					nameof(scheme),
					_scheme);

			_domain =
				INISetting.GetValueWithAdd(
					DataBaseAPI.DATA_BASE_SETTINGS_GROUP_NAME,
					nameof(domain),
					_domain);

			_port =
				INISetting.GetValueWithAdd(
					DataBaseAPI.DATA_BASE_SETTINGS_GROUP_NAME,
					nameof(port),
					_port);

			_APIRootPath =
				INISetting.GetValueWithAdd(
					DataBaseAPI.DATA_BASE_SETTINGS_GROUP_NAME,
					nameof(APIRootPath),
					_APIRootPath);
		}
	}
}