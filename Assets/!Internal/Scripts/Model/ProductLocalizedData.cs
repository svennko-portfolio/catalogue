using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using Newtonsoft.Json;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model
{
	[Serializable]
	public class ProductLocalizedData : CatalogueItemLocalizedData, ProductLocalizedDataInterface
	{
		[SerializeField]
		[Multiline(15)]
		private string _fullDescription;

		[JsonProperty("full_description", Order = 4)]
		public string fullDescription
		{
			get => _fullDescription;
			internal set => _fullDescription = value;
		}

		[SerializeField]
		[JsonProperty("photo_urls", Order = 5)]
		private string[] _photosURLs = new string[0];

		[JsonIgnore]
		public IReadOnlyList<string> photosURLs
		{
			get => _photosURLs;
			internal set => _photosURLs = value.ToArray();
		}

		IReadOnlyList<string> ProductLocalizedDataInterface.photosURLs
		{
			get => _photosURLs;
		}

		[SerializeField]
		private Sprite[] _photos;

		[JsonIgnore]
		public IReadOnlyList<Sprite> photos
		{
			get => _photos;
			internal set => _photos = value.ToArray();
		}
		IReadOnlyList<Sprite> ProductLocalizedDataInterface.photos
		{
			get => _photos;
			set => _photos = value.ToArray();
		}

		[SerializeField]
		[JsonProperty("video_urls", Order = 6)]
		private string[] _videosURLs = new string[0];

		[JsonIgnore]
		public IReadOnlyList<string> videosURLs
		{
			get => _videosURLs;
			internal set => _videosURLs = value.ToArray();
		}
		IReadOnlyList<string> ProductLocalizedDataInterface.videosURLs
		{
			get => _videosURLs;
			set => _videosURLs = value.ToArray();
		}

		[SerializeField]
		private string _productWebsiteQRCodeURL = string.Empty;

		[JsonProperty("product_url", Order = 7)]
		public string productWebsiteQRCodeURL
		{
			get => _productWebsiteQRCodeURL;
			internal set => _productWebsiteQRCodeURL = value;
		}
		string ProductLocalizedDataInterface.productWebsiteQRCodeURL => _productWebsiteQRCodeURL;

		[SerializeField]
		private Sprite _productWebsiteQRCode;

		[JsonIgnore]
		public Sprite productWebsiteQRCode
		{
			get => _productWebsiteQRCode;
			internal set => _productWebsiteQRCode = value;
		}
		Sprite ProductLocalizedDataInterface.productWebsiteQRCode
		{
			get => _productWebsiteQRCode;
			set => _productWebsiteQRCode = value;
		}

		[SerializeField]
		private string _managerQRCodeURL = string.Empty;

		[JsonProperty("manager_url", Order = 8)]
		public string managerQRCodeURL
		{
			get => _managerQRCodeURL;
			internal set => _managerQRCodeURL = value;
		}
		string ProductLocalizedDataInterface.managerQRCodeURL => _managerQRCodeURL;

		[SerializeField]
		private Sprite _managerQRCode;

		[JsonIgnore]
		public Sprite managerQRCode
		{
			get => _managerQRCode;
			internal set => _managerQRCode = value;
		}

		Sprite ProductLocalizedDataInterface.managerQRCode
		{
			get => _managerQRCode;
			set => _managerQRCode = value;
		}

		public override string ToString() =>
			$"{base.ToString()}\n\t" +
				$"{nameof(CategoryMainData)} " +
				$"{{ " +
					$"{nameof(fullDescription)}: {fullDescription}; " +
					$"{nameof(photosURLs)} count: {photosURLs.Count}; " +
					$"{nameof(videosURLs)} count: {videosURLs.Count}; " +
				$"}}";
	}
}