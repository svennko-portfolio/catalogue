﻿using System;

using UnityEngine;
using Sirenix.OdinInspector;

namespace ICD.Catalogue.Model.DataLoading
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Data/API/Data Base Host", fileName = "Data Base Host")]
	public class DataBaseAPIHost : ScriptableObject
	{
		[SerializeField]
		[Required]
		protected string _scheme = "http";
		public string scheme => _scheme;

		[SerializeField]
		[Required]
		protected string _domain = "127.0.0.1";
		public string domain => _domain;

		[SerializeField]
		protected ushort _port = 8000;
		public ushort port => _port;

		[SerializeField]
		protected string _APIRootPath;
		public string APIRootPath => _APIRootPath;

		public virtual UriBuilder APIURLBuilder =>
			new UriBuilder(
				_scheme,
				_domain,
				_port,
				_APIRootPath);
	}
}