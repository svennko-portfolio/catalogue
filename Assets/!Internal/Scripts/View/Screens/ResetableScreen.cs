using System.Collections;

using UnityEngine;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Utility;

using ICD.Catalogue.Controller;

namespace ICD.Catalogue.View.Screens
{
	[RequireComponent(typeof(PresentationScreen))]
	[DisallowMultipleComponent]
	public class ResetableScreen : MonoBehaviour
	{
		[SerializeField]
		[Min(0)]
		private float _resetTimeout = 60;

		[SerializeField]
		[Required]
		[AssetsOnly]
		private PresentationScreen _defaultScreenPrefab;

		private PresentationScreen _screen;
		internal PresentationScreen screen
		{
			get
			{
				if (!_screen)
					_screen = GetComponent<PresentationScreen>();
				return _screen;
			}
		}

		private Coroutine _resetCoroutine;

		void HandleAnyAction()
		{
			if (_resetCoroutine != null)
				StopCoroutine(
					_resetCoroutine);

			_resetCoroutine =
				StartCoroutine(
					WaitForReset());
		}

		IEnumerator WaitForReset()
		{
			yield return new WaitForSecondsRealtime(_resetTimeout);

			screen?.ToScreen(
				_defaultScreenPrefab);
			PresentationController.instance.HandleReturnToStartScreen();

			_resetCoroutine = null;
		}

		void Awake()
		{
			INISetting.ReloadINI();

			_resetTimeout =
				INISetting.GetValueWithAdd(
					ApplizationInitializer.APPLICATION_SETTINGS_GROUP,
					"Reset Timeout",
					(int)(_resetTimeout * 1000)) / 1000f;

			InputHandler.instance.AnyActionPerformed += HandleAnyAction;

			_resetCoroutine=
				StartCoroutine(
					WaitForReset());
		}

		void OnDestroy()
		{
			InputHandler.instance.AnyActionPerformed -= HandleAnyAction;
		}
	}
}