﻿using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.View.Interfaces
{
	public interface CatalogueItemKeeper
	{
		public CatalogueItemInterface data { get; set; }
	}
}
