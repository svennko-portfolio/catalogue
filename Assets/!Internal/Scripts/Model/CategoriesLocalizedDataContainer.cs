using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Localization;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Utility;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Data/Containers/Cotegories Localized Data", fileName = "Cotegories Localized Data Container")]
	public class CategoriesLocalizedDataContainer : SerializedScriptableObject, WritableCategoriesLocalizedDataContainerInterface
	{
		[SerializeField]
		private Dictionary<LocalizedDataKey, CategoryLocalizedData> _categoriesLocalizedData =
			new Dictionary<LocalizedDataKey, CategoryLocalizedData>();

		public IEnumerable<CategoryLocalizedDataInterface> all => _categoriesLocalizedData.Values;
		IEnumerable<CatalogueItemInterface> CatalogueItemsLocalizedDataContainerInterface.all =>
			all as IEnumerable<CatalogueItemInterface>;

		public CategoriesLocalizedDataContainer()
		{
			Application.quitting += ClearData;
		}

		public bool TryGetLocalizedData(uint categoryID, string localeID, out Interfaces.CategoryLocalizedDataInterface localizedData)
		{
			localizedData = null;
			if (_categoriesLocalizedData.TryGetValue(
					new LocalizedDataKey(
						categoryID,
						localeID),
					out CategoryLocalizedData categoryLocalizedData))
			{
				localizedData = categoryLocalizedData;
				return true;
			}
			return false;
		}

		public bool TryGetLocalizedDataForCurrentLocale(uint categoryID, out Interfaces.CategoryLocalizedDataInterface localizedData) =>
			TryGetLocalizedData(
				categoryID,
				LocalizationHelper.currentLocaleID,
				out localizedData);

		public bool TryAddLocalizedData(uint categoryID, string localeID, Interfaces.CategoryLocalizedDataInterface localizedData, bool owerwriteIfExists = true)
		{
			if (localizedData == null)
				return false;

			var key =
				new LocalizedDataKey(
					categoryID,
					localeID);

			if (_categoriesLocalizedData.ContainsKey(key) && !owerwriteIfExists)
				return false;
			else
			if (!_categoriesLocalizedData.ContainsKey(key))
			{
				_categoriesLocalizedData.Add(
					key,
					localizedData is CategoryLocalizedData ?
						localizedData as CategoryLocalizedData :
						new CategoryLocalizedData()
						{
							name = localizedData.name,
							shortDescription = localizedData.shortDescription,
							logoURL = localizedData.logoURL,
							logo = localizedData.logo
						});
				return true;
			}
			else
			if (owerwriteIfExists)
			{
				_categoriesLocalizedData[key] =
					localizedData is CategoryLocalizedData ?
						localizedData as CategoryLocalizedData :
						new CategoryLocalizedData()
						{
							name = localizedData.name,
							shortDescription = localizedData.shortDescription,
							logoURL = localizedData.logoURL,
							logo = localizedData.logo
						};
				return true;
			}

			return false;
		}

		[ContextMenu("Clear Data")]
		public void ClearData()
		{
			foreach (var categoryLocalizedData in _categoriesLocalizedData.Values)
				categoryLocalizedData.logo.DestroyByApplicationPlayingState();

			_categoriesLocalizedData.Clear();
		}

		bool CheckCatalogueItemType(CatalogueItemLocalizedDataInterface catalogueItemLocalizedData)
		{
			if (!(catalogueItemLocalizedData is CategoryLocalizedDataInterface))
				throw new InvalidCastException(
					$"Value must implement {nameof(CategoryLocalizedDataInterface)} interface");

			return true;
		}


		bool CatalogueItemsLocalizedDataContainerInterface.TryGetLocalizedData(uint itemID, string localeID, out Interfaces.CatalogueItemLocalizedDataInterface localizedData)
		{
			localizedData = null;
			if (TryGetLocalizedData(
						itemID,
						localeID,
						out CategoryLocalizedDataInterface categoryLocalizedData))
			{
				localizedData = categoryLocalizedData;
				return true;
			}
			return false;
		}

		bool CatalogueItemsLocalizedDataContainerInterface.TryGetLocalizedDataForCurrentLocale(uint itemID, out CatalogueItemLocalizedDataInterface localizedData) =>
			(this as CatalogueItemsLocalizedDataContainerInterface).TryGetLocalizedData(
				itemID,
				LocalizationHelper.currentLocaleID,
				out localizedData);

		bool WritableCatalogueItemsLocalizedDataContainerInterface.TryAddLocalizedData(uint itemID, string localeID, CatalogueItemLocalizedDataInterface localizedData, bool owerwriteIfExists) =>
			TryAddLocalizedData(
				itemID,
				localeID,
				localizedData as CategoryLocalizedDataInterface,
				owerwriteIfExists);
	}
}