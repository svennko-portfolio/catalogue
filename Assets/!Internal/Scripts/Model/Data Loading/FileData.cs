﻿using System;

using UnityEngine;
using Newtonsoft.Json;

namespace ICD.Catalogue.Model.DataLoading
{
	[Serializable]
	public struct FileData
	{
		[SerializeField]
		private uint _ID;

		[JsonProperty("id", Order = 1)]
		public uint ID
		{
			get => _ID;
			internal set => _ID = value;
		}
	}
}
