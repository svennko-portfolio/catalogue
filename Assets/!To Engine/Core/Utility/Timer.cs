using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ICD.Engine.Core.Utility
{
	public class Timer : MonoBehaviour
	{
		[SerializeField]
		[Min(.001f)]
		private uint _time;
		public uint time
		{
			get => _time;
			set => _time = value;
		}

		[SerializeField]
		private UnityEvent _onTimeUp;

		[SerializeField]
		private bool _repeat;
		public bool repeat
		{
			get => _repeat;
			set
			{
				_repeat = value;
				if (enabled && _timerCoroutine == null)
					StartTimer();
			}
		}

		[SerializeField]
		private bool _autoStartOnEnable;

		[SerializeField]
		private bool _realTime;
		public void StartTimer()
		{
			StopTimer();
			_timerCoroutine = StartCoroutine(TimerCoroutine());
		}
		private Coroutine _timerCoroutine;
		IEnumerator TimerCoroutine()
		{
			do
			{
				if (_realTime)
					yield return new WaitForSecondsRealtime(_time);
				else
					yield return new WaitForSeconds(_time);

				_onTimeUp?.Invoke();
			}
			while (_repeat);
			_timerCoroutine = null;
		}
		public void StopTimer()
		{
			if (_timerCoroutine != null)
			{
				StopCoroutine(_timerCoroutine);
				_timerCoroutine = null;
			}
		}
		void OnEnable()
		{
			if (_timerCoroutine == null && _autoStartOnEnable)
				StartTimer();
		}
		void OnDisable()
		{
			StopTimer();
		}
	}
}
