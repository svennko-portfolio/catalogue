using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using Newtonsoft.Json;
using ICD.Engine.Core.Utility;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model.DataLoading.Interfaces
{
	internal interface DataBaseAPI
	{
		public const string DATA_BASE_SETTINGS_GROUP_NAME = "DATA BASE";

		public bool isLoggedIn { get; }

		public Task<bool> LogIn(string username, string password, CancellationToken cancellationToken = default);
		public Task<bool> LogIn(DataBaseCredentials credentials, CancellationToken cancellationToken = default);
		public Task<bool> LogOut(CancellationToken cancellationToken = default);

		public Task<IEnumerable<CategoryMainDataInterface>> GetAllCategories(CancellationToken cancellationToken = default);
		public Task<CategoryMainDataInterface> GetCategoriy(uint categoryID, CancellationToken cancellationToken = default);

		public Task<IEnumerable<ProductMainDataInterface>> GetAllProducts(CancellationToken cancellationToken = default);
		public Task<ProductMainDataInterface> GetProduct(uint productID, CancellationToken cancellationToken = default);

		public Task<IReadOnlyDictionary<string, CategoryLocalizedDataInterface>> GetCategoryLocalizedData(uint categoryID, CancellationToken cancellationToken = default);
		public Task<CategoryLocalizedDataInterface> GetCategoryLocalizedData(uint categoryID, string localeID, CancellationToken cancellationToken = default);

		public Task<IReadOnlyDictionary<string, ProductLocalizedDataInterface>> GetProductLocalizedData(uint productID, CancellationToken cancellationToken = default);
		public Task<ProductLocalizedDataInterface> GetProductLocalizedData(uint productID, string localeID, CancellationToken cancellationToken = default);

		public Task<long> GetLastFileChangesTimestamp(string fileURI);
	}

	[Serializable]
	public struct DataBaseCredentials
	{
		public const string
			USERNAME_PROPERTY_NAME = "username",
			PASSWORD_PROPERTY_NAME = "password";

		[SerializeField]
		private string
			_username,
			_password;

		[JsonProperty(USERNAME_PROPERTY_NAME)]
		public string username
		{
			get => _username;
			set => _username = value;
		}

		[JsonProperty(PASSWORD_PROPERTY_NAME)]
		public string password
		{
			get => _password;
			set => _password = value;
		}

		public static DataBaseCredentials FromSettings()
		{
			INISetting.ReloadINI();

			return
				new DataBaseCredentials()
				{
					username =
						INISetting.GetValueWithAdd(
							DataBaseAPI.DATA_BASE_SETTINGS_GROUP_NAME,
							USERNAME_PROPERTY_NAME,
							"administrator"),
					password =
						INISetting.GetValueWithAdd(
							DataBaseAPI.DATA_BASE_SETTINGS_GROUP_NAME,
							PASSWORD_PROPERTY_NAME,
							"asd567fgh"),
				};
		}
	}
}
