using System.Collections.Generic;

namespace ICD.Catalogue.Model.Interfaces
{
	public interface CatalogueItemsLocalizedDataContainerInterface
	{
		public IEnumerable<CatalogueItemInterface> all { get; }
		public bool TryGetLocalizedData(uint itemID, string localeID, out CatalogueItemLocalizedDataInterface localizedData);
		public bool TryGetLocalizedDataForCurrentLocale(uint itemID, out CatalogueItemLocalizedDataInterface localizedData);
	}

	internal interface WritableCatalogueItemsLocalizedDataContainerInterface : CatalogueItemsLocalizedDataContainerInterface
	{
		public bool TryAddLocalizedData(uint itemID, string localeID, CatalogueItemLocalizedDataInterface localizedData, bool owerwriteIfExists = true);
		public void ClearData();
	}
}