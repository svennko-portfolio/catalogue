using System;

using UnityEngine;
using Newtonsoft.Json;

using ICD.Catalogue.Model.Interfaces;
using ContentType = ICD.Catalogue.Model.Interfaces.CategoryMainDataInterface.ContentType;

namespace ICD.Catalogue.Model
{
	[Serializable]
	public class CategoryMainData : CatalogueItemMainData, CategoryMainDataInterface
	{
		[SerializeField]
		private ContentType _contentType;

		[JsonProperty("content_type", Required = Required.Always, Order = 5)]
		public ContentType contentType
		{
			get => _contentType;
			internal set => _contentType = value;
		}

		public override string ToString() =>
			$"{base.ToString()}\n\t" +
				$"{nameof(CategoryMainData)} " +
				$"{{ " +
					$"{nameof(contentType)}: {contentType}; " +
				$"}}";
	}
}