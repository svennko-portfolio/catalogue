using System;

using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

using ICD.Catalogue.Model.Interfaces;
using ICD.Catalogue.View.Screens;

namespace ICD.Catalogue.View
{
	public abstract class PageContent : MonoBehaviour
	{
		[SerializeField]
		[ReadOnly]
		private ContentScreen _owner;
		public ContentScreen owner
		{
			get => _owner;
			set => _owner = value;
		}

		[SerializeField]
		private string _disappearanceTriggerName;

		public event Action<PageContent> Appeared;

		public abstract CatalogueItemInterface data { get; }

		public abstract void Construct(CatalogueItemInterface data);

		public void SetContentAppeared()
		{
			Appeared?.Invoke(
				this);
		}
		public void Disappear()
		{
			GetComponent<Animator>()?.SetTrigger(
				_disappearanceTriggerName);
		}
	}
}