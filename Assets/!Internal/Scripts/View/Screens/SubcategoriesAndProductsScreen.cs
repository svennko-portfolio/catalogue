using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.View.Screens
{
	public class SubcategoriesAndProductsScreen : ContentScreen
	{
		[SerializeField]
		[SceneObjectsOnly]
		private PageHeaderController _headerController;

		[SerializeField]
		private UnityEvent _returnToRootReqired;

		[SerializeField]
		private BooleanEvent _categoryPageOpened;

		private Stack<CatalogueItemInterface> _itemsTrace =
			new Stack<CatalogueItemInterface>(
				3);

		public override void GoBack()
		{
			if (_itemsTrace.Count == 0)
			{
				selector.enabled = false;
				_returnToRootReqired?.Invoke();
				return;
			}

			SwapPageContent(
				_itemsTrace.Pop(),
				false);
		}

		public override void GoForward(CatalogueItemInterface itemData)
		{
			if (currentContent?.data == itemData)
				return;

			if (currentContent?.data != null)
				_itemsTrace.Push(
					currentContent.data);

			SwapPageContent(
				itemData,
				true);
		}

		void SwapPageContent(CatalogueItemInterface itemData, bool forwardOrBack)
		{
			Model.ModelAgregator.instance.lastRequiredItem = itemData;
			currentContent?.Disappear();

			if (selector)
				selector.isActive = false;

			ConstructPageContent(
				itemData);

			_headerController?.SetNewHeader(
				itemData,
				forwardOrBack);

			if (selector)
				selector.isVisible = !(itemData is ProductInterface);

			_categoryPageOpened?.Invoke(
				itemData is CategoryInterface);
		}

		void Awake()
		{
			SwapPageContent(
				Model.ModelAgregator.instance.lastRequiredItem,
				true);
		}
	}
}