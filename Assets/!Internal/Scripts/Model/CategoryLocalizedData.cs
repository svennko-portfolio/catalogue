using System;

namespace ICD.Catalogue.Model
{
	[Serializable]
	public class CategoryLocalizedData : CatalogueItemLocalizedData, Interfaces.CategoryLocalizedDataInterface
	{ }
}