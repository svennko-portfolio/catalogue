using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine.Localization.Settings;

namespace UnityEngine.Localization
{
	public static class LocalizationHelper
	{
		public static string currentLocaleID =>
			LocalizationSettings.SelectedLocale.Identifier.Code;

		public static IEnumerable<string> availableLocales =>
			from locale in LocalizationSettings.AvailableLocales.Locales
			where locale
			select locale.Identifier.Code;

		public static int availableLocalesLocalesCount =>
			LocalizationSettings.AvailableLocales.Locales.Count;

		public static bool IsAvailableLocaleID(this string localeID) =>
			availableLocales.Contains(
				localeID);

		public static Locale AsAvailableLocale(this string localeID)
		{
			foreach (var locale in LocalizationSettings.AvailableLocales.Locales)
				if (locale.Identifier.Code == localeID)
					return locale;

			return null;
		}

		public static bool SetCurrentLocale(string localeID)
		{
			if (localeID == null)
				return false;

			var locale = localeID.AsAvailableLocale();
			if (locale)
			{
				LocalizationSettings.SelectedLocale = locale;
				return true;
			}
			else
			{
				Debug.LogWarning(
					$"Locale with ID \"{localeID}\" is not available");
				return false;
			}
		}
	}
}
