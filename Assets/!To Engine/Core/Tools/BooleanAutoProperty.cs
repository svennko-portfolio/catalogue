using System;

using UnityEngine;
using UnityEngine.Events;

namespace ICD.Engine.Core.Tools
{
	[Serializable]
	public class BooleanAutoProperty : AutoProperty<bool>
	{
		[SerializeField]
		private BooleanEvent _valueChanged;

		public BooleanAutoProperty()
		{
			ValueChanged +=
				value =>
					_valueChanged?.Invoke(
						value);
		}
	}
}
