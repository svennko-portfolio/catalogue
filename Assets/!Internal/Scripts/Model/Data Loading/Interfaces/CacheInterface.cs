﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using Newtonsoft.Json;

namespace ICD.Catalogue.Model.DataLoading.Interfaces
{
	internal interface CacheInterface
	{
		[Serializable]
		public struct CacheMetaData
		{
			[SerializeField]
			private string _downloadURL;

			[JsonProperty("Download URL")]
			public string downloadURL
			{
				get => _downloadURL;
				set => _downloadURL = value;
			}

			[SerializeField]
			private string _localCahcedFilePath;

			[JsonProperty("Cached File Path")]
			public string localCahcedFilePath
			{
				get => _localCahcedFilePath;
				set
				{
					_localCahcedFilePath = value;
					_cachedFile =
						new FileInfo(
							_localCahcedFilePath);
				}
			}

			private FileInfo _cachedFile;

			public bool isFileExistInCache
			{
				get
				{
					if (_cachedFile == null)
						_cachedFile =
							new FileInfo(
								_localCahcedFilePath);

					return _cachedFile.Exists;
				}
			}
		}

		public DirectoryInfo directory { get; }
		
		public bool TryGetCachedFile(string identifier, out FileInfo cachedFilePath);

		public void ClearData();
		public void RemoveCache();
	}
}