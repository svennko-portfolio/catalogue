
using System;

using UnityEngine.UI;

namespace UnityEngine.Events
{
	#region System
	[Serializable]
	public class ObjectEvent : UnityEvent<object> { }

	[Serializable]
	public class BooleanEvent : UnityEvent<bool> { }

	[Serializable]
	public class ByteEvent : UnityEvent<byte> { }

	[Serializable]
	public class ShortEvent : UnityEvent<short> { }

	[Serializable]
	public class UnsignedShortEvent : UnityEvent<ushort> { }

	[Serializable]
	public class IntegerEvent : UnityEvent<int> { }

	[Serializable]
	public class UnsignedIntegerEvent : UnityEvent<uint> { }

	[Serializable]
	public class FloatEvent : UnityEvent<float> { }

	[Serializable]
	public class DoubleEvent : UnityEvent<double> { }

	[Serializable]
	public class DecimalEvent : UnityEvent<decimal> { }

	[Serializable]
	public class CharacterEvent : UnityEvent<char> { }

	[Serializable]
	public class StringEvent : UnityEvent<string> { }
	#endregion

	#region Unity
	#region Base Types
	[Serializable]
	public class UnityObjectEvent : UnityEvent<Object> { }

	[Serializable]
	public class GameObjectEvent : UnityEvent<GameObject> { }

	[Serializable]
	public class ScriptableObjectEvent : UnityEvent<ScriptableObject> { }

	[Serializable]
	public class ComponentEvent : UnityEvent<Component> { }

	[Serializable]
	public class BehaviourEvent : UnityEvent<Behaviour> { }

	[Serializable]
	public class MonoBehaviourEvent : UnityEvent<MonoBehaviour> { }

	[Serializable]
	public class TransformEvent : UnityEvent<Transform> { }

	[Serializable]
	public class RectTransformEvent : UnityEvent<RectTransform> { }
	#endregion

	#region Math
	[Serializable]
	public class Vector2Event : UnityEvent<Vector2> { }

	[Serializable]
	public class Vector2IntEvent : UnityEvent<Vector2Int> { }

	[Serializable]
	public class Vector3Event : UnityEvent<Vector3> { }

	[Serializable]
	public class Vector3IntEvent : UnityEvent<Vector3Int> { }

	[Serializable]
	public class Vector4Event : UnityEvent<Vector4> { }

	[Serializable]
	public class RectEvent : UnityEvent<Rect> { }

	[Serializable]
	public class RectIntEvent : UnityEvent<RectInt> { }

	[Serializable]
	public class QuaternionEvent : UnityEvent<Quaternion> { }

	[Serializable]
	public class Matrix4x4Event : UnityEvent<Matrix4x4> { }
	#endregion

	#region Phisics
	[Serializable]
	public class ColliderEvent : UnityEvent<Collider> { }

	[Serializable]
	public class Collider2DEvent : UnityEvent<Collider2D> { }

	[Serializable]
	public class RigidbodyEvent : UnityEvent<Rigidbody> { }

	[Serializable]
	public class Rigidbody2DEvent : UnityEvent<Rigidbody2D> { }
	#endregion

	#region Rendering
	[Serializable]
	public class TextureEvent : UnityEvent<Texture> { }

	[Serializable]
	public class Texture2DEvent : UnityEvent<Texture2D> { }

	[Serializable]
	public class RenderTextureEvent : UnityEvent<RenderTexture> { }

	[Serializable]
	public class SpriteEvent : UnityEvent<Sprite> { }

	[Serializable]
	public class RendererEvent : UnityEvent<Renderer> { }
	#endregion

	#region UI
	[Serializable]
	public class CanvasEvent : UnityEvent<Canvas> { }

	[Serializable]
	public class CanvasGroupEvent : UnityEvent<CanvasGroup> { }

	[Serializable]
	public class LayoutGroupEvent : UnityEvent<LayoutGroup> { }

	[Serializable]
	public class HorizontalLayoutGroupEvent : UnityEvent<HorizontalLayoutGroup> { }

	[Serializable]
	public class VerticalLayoutGroupEvent : UnityEvent<VerticalLayoutGroup> { }

	[Serializable]
	public class GridLayoutGroupEvent : UnityEvent<GridLayoutGroup> { }

	[Serializable]
	public class GraphicEvent : UnityEvent<Graphic> { }

	[Serializable]
	public class RawImageEvent : UnityEvent<RawImage> { }

	[Serializable]
	public class ImageEvent : UnityEvent<Image> { }

	[Serializable]
	public class TextEvent : UnityEvent<Text> { }

	[Serializable]
	public class ToggleEvent : UnityEvent<Toggle> { }

	[Serializable]
	public class ToggleGroupEvent : UnityEvent<ToggleGroup> { }

	[Serializable]
	public class DropdownEvent : UnityEvent<Dropdown> { }

	[Serializable]
	public class InputFieldEvent : UnityEvent<InputField> { }

	[Serializable]
	public class SliderEvent : UnityEvent<Slider> { }

	[Serializable]
	public class ScrollbarEvent : UnityEvent<Scrollbar> { }

	[Serializable]
	public class ScrollRectEvent : UnityEvent<ScrollRect> { }
	#endregion

	#endregion
}
