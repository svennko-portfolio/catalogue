using UnityEngine;

namespace ICD.Engine.Core.Animations
{
	public sealed class AnimatorBoolSetter : AnimatorParameterSetter<bool>
	{
		protected override bool GetParameterFromAnimator() =>
			animator.GetBool(
				parameterName);
		protected override void SetParameter(bool parameterValue)
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
#endif
				animator.SetBool(
					parameterName,
					parameterValue);
		}
	}
}