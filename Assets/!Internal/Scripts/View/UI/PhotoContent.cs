﻿using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace ICD.Catalogue.View
{
	[RequireComponent(typeof(AspectRatioFitter))]
	public class PhotoContent : MonoBehaviour
	{
		[SerializeField]
		[Required]
		[SceneObjectsOnly]
		private Image _photoImage;

		private AspectRatioFitter _aspectRatioFitter;
		private AspectRatioFitter aspectRatioFitter
		{
			get
			{
				if (!_aspectRatioFitter)
					_aspectRatioFitter = GetComponent<AspectRatioFitter>();
				return _aspectRatioFitter;
			}
		}

		public Sprite photo
		{
			get => _photoImage?.sprite;
			set
			{
				gameObject.SetActive(value);
				if (value)
					aspectRatioFitter.aspectRatio =
						value.textureRect.width /
						value.textureRect.height;
				_photoImage.sprite = value;
			}
		}
	}
}
