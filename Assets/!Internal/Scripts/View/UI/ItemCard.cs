﻿using System;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

using Sirenix.OdinInspector;

using ICD.Catalogue.View.Interfaces;
using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.View
{
	public class ItemCard : MonoBehaviour, CatalogueItemKeeper, SelectableItemInterface
	{
		[SerializeField]
		[SceneObjectsOnly]
		private TMP_Text
			_nameField,
			_shortDescriptionField;

		[SerializeField]
		[SceneObjectsOnly]
		private Image
			_iconImage,
			_logoImage;

		private CatalogueItemInterface _data;
		public CatalogueItemInterface data
		{
			get => _data;
			set
			{
				if (_nameField)
					_nameField.text = value?.localizedData?.name;
				if (_shortDescriptionField)
					_shortDescriptionField.text = value?.localizedData?.shortDescription;

				if (_iconImage)
					_iconImage.sprite = value?.mainData?.icon;
				if (_logoImage)
					_logoImage.sprite = value?.localizedData?.logo;

				_data = value;
			}
		}


		Rect SelectableItemInterface.rect
		{
			get
			{
				var rectTransorm = transform as RectTransform;
				if (rectTransorm)
					return rectTransorm.rect;

				return Rect.zero;
			}
		}
		Vector2 SelectableItemInterface.pivot
		{
			get
			{
				var rectTransorm = transform as RectTransform;
				if (rectTransorm)
					return rectTransorm.pivot;

				return new Vector2(.5f, .5f);
			}
		}
		Vector2 SelectableItemInterface.anchoredPosition
		{
			get
			{
				var rectTransorm = transform as RectTransform;
				if (rectTransorm)
					return rectTransorm.anchoredPosition;

				return transform.localPosition;
			}
		}
		Vector3 SelectableItemInterface.position => transform.position;
		Matrix4x4 SelectableItemInterface.worldToLocalMatrix => transform.worldToLocalMatrix;
		Matrix4x4 SelectableItemInterface.localToWorldMatrix => transform.localToWorldMatrix;

		[SerializeField]
		private UnityEvent _selected;

		public event Action<ItemCard> Selected;

		public ItemCard()
		{
			Selected += HandleSelectionEvent;
		}

		public void Select()
		{
			Selected?.Invoke(
				this);
		}

		void HandleSelectionEvent(ItemCard item) =>
			_selected?.Invoke();
	}
}
