﻿using UnityEngine;

namespace ICD.Catalogue.View.Interfaces
{
	public interface SelectableItemInterface
	{
		public Rect rect { get; }
		public Vector2 pivot { get; }
		public Vector2 anchoredPosition { get; }
		public Vector3 position { get; }
		public Matrix4x4 worldToLocalMatrix { get; }
		public Matrix4x4 localToWorldMatrix { get; }
		public void Select();
	}
}
