using UnityEngine;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Utility;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.View.Screens
{
    public class RootCategoriesScreen : ContentScreen
	{
		[SerializeField]
		[AssetsOnly]
		[Required]
		private PresentationScreen
			_forwardScreen,
			_backwardScreen;

		public RootCategoriesScreen()
		{
			ActivationStateChanged += HandleActivationStateChange;
		}

		public override void GoBack()
		{
			if (selector)
			{
				selector.isVisible = false;
				selector.enabled = false;
			}

			ToScreen(
				_backwardScreen);
		}

		public override void GoForward(CatalogueItemInterface item)
		{
			Model.ModelAgregator.instance.lastRequiredItem = item;
			if (selector)
			{
				selector.isVisible = false;
				selector.enabled = false;
			}

			ToScreen(
				_forwardScreen);
		}

		void HandleActivationStateChange(bool newState) =>
			selector.isActive = newState;

		void Awake()
		{
			if (currentContent)
				currentContent.DestroyByApplicationPlayingState();

			ConstructPageContent(
				null);
		}
	}
}