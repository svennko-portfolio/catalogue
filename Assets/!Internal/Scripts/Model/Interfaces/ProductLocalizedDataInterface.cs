using System.Collections.Generic;

using UnityEngine;

namespace ICD.Catalogue.Model.Interfaces
{
	public interface ProductLocalizedDataInterface : CatalogueItemLocalizedDataInterface
	{
		public string fullDescription { get; }
		internal string productWebsiteQRCodeURL { get; }
		public Sprite productWebsiteQRCode { get; internal set; }
		internal string managerQRCodeURL { get; }
		public Sprite managerQRCode { get; internal set; }
		internal IReadOnlyList<string> photosURLs { get; }
		public IReadOnlyList<Sprite> photos { get; internal set; }
		public IReadOnlyList<string> videosURLs { get; internal set; }
	}
}