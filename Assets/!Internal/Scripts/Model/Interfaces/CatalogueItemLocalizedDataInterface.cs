using UnityEngine;

namespace ICD.Catalogue.Model.Interfaces
{
	public interface CatalogueItemLocalizedDataInterface
	{
		public string name { get; }
		public string shortDescription { get; }
		internal string logoURL { get; }
		public Sprite logo { get; internal set; }
		public bool hideNameInHeader { get; }
	}
}