﻿
using UnityEngine;
using UnityEngine.UI;

namespace ICD.Catalogue.View.Interfaces
{
	public interface SelectionBehaviourInterface
	{
		public SelectableItemInterface Process(RectTransform selectionHandler, ScrollRect scroll);
		public void ProcessInaction(RectTransform selectionHandler, ScrollRect scroll);
	}
}
