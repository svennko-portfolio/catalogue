using System;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model
{
	[Serializable]
	public abstract class CatalogueItem : CatalogueItemInterface
	{
		public CatalogueItemMainDataInterface mainData
		{
			get => baseMainData;
			internal set => baseMainData = value;
		}
		protected abstract CatalogueItemMainDataInterface baseMainData { get; set; }

		public CatalogueItemLocalizedDataInterface localizedData =>
			_localizedDataContainer.TryGetLocalizedDataForCurrentLocale(mainData.ID, out CatalogueItemLocalizedDataInterface localizedData) ?
				localizedData :
				null;

		protected CatalogueItemsLocalizedDataContainerInterface _localizedDataContainer { get; }

		public CatalogueItem(CatalogueItemsLocalizedDataContainerInterface localizedDataContainer)
		{
			if (localizedDataContainer == null)
				throw new ArgumentNullException(
					nameof(localizedDataContainer));

			_localizedDataContainer = localizedDataContainer;
		}
	}
}