using System;
using System.Collections.Generic;
using System.Text;

namespace ICD.Catalogue.Model.Utility
{
	public static class TextBeautyfier
	{
		const string
			START_TAG = "<nobr>",
			END_TAG = "</nobr>";
		const int MINIMAL_LENGTH = 4;

		public static string Beautyfy(this string text)
		{
			if (string.IsNullOrWhiteSpace(text))
				return string.Empty;

			var paragraphs =
				text.Split(
					'\n',
					StringSplitOptions.None);

			for (int p = 0; p < paragraphs.Length; p++)
				paragraphs[p] =
					paragraphs[p].BeautyfyParagraph();

			var builder = new StringBuilder(text.Length);
			foreach (var paragraph in paragraphs)
				builder.AppendLine(
					paragraph);

			return builder.ToString();
		}

		static string BeautyfyParagraph(this string paragraph)
		{
			var words =
				paragraph.Split(
					' ',
					StringSplitOptions.None);

			if (string.IsNullOrWhiteSpace(paragraph) ||
				words.Length == 1)
				return paragraph;

			var startTagsIndexes = new HashSet<int>(words.Length);
			var endTagsIndexes = new HashSet<int>(words.Length);

			for (int w = words.Length - 1; w >= 0; w--)
			{

			}

			var builder = new StringBuilder(paragraph.Length);
			for (int w = 0; w < words.Length; w++)
			{
				if (startTagsIndexes.Contains(w))
					builder.Append(START_TAG);

				builder.Append(words[w]);

				if (endTagsIndexes.Contains(w))
					builder.Append(END_TAG);

				if (w < words.Length - 1)
					builder.Append(' ');
			}

			return builder.ToString();
		}
	}
}