using System;

using UnityEngine;
using Sirenix.OdinInspector;
using Newtonsoft.Json;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model
{
	[Serializable]
	public abstract class CatalogueItemMainData : CatalogueItemMainDataInterface
	{
		[SerializeField]
		private uint _ID;

		[JsonProperty("id", Required = Required.Always, Order = 1)]
		public uint ID
		{
			get => _ID;
			internal set => _ID = value;
		}

		[SerializeField]
		private bool _hasParent;

		[SerializeField]
		[ShowIf("_hasParent")]
		private uint _parentID;

		[JsonProperty("parent_id", Required = Required.AllowNull, Order = 2)]
		public uint? parentID
		{
			get =>
				_hasParent ?
					_parentID :
					null;
			internal set
			{
				_hasParent = value.HasValue;
				if (value.HasValue)
					_parentID = value.Value;
			}
		}

		[SerializeField]
		private uint _orderNumber = 1;

		[JsonProperty("order_number", Order = 3)]
		public uint orderNumber
		{
			get => _orderNumber;
			internal set => _orderNumber = value;
		}

		[SerializeField]
		private Sprite _icon;

		[JsonIgnore]
		public Sprite icon
		{
			get => _icon;
			internal set => _icon = value;
		}

		Sprite CatalogueItemMainDataInterface.icon
		{
			get => _icon;
			set => _icon = value;
		}

		[SerializeField]
		[JsonProperty("icon_URL", Order = 4)]
		private string _iconURL;
		internal string iconURL
		{
			get => _iconURL;
			set => _iconURL = value;
		}

		string CatalogueItemMainDataInterface.iconURL => _iconURL;

		public override string ToString() =>
			$"{nameof(CatalogueItemMainData)} " +
			$"{{ " +
				$"{nameof(ID)}: {ID}; " +
				$"{nameof(parentID)}: {parentID}; " +
				$"{nameof(orderNumber)}: {orderNumber}; " +
				$"{nameof(iconURL)}: {iconURL}; " +
			$"}} ";
	}
}