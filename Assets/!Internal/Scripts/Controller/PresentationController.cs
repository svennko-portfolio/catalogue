using System.Threading;

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Singletons;

using ICD.Catalogue.Model;
using FilesLoadingPolicy = ICD.Catalogue.Model.DataLoading.Interfaces.DataLoaderInterface.FilesLoadingPolicy;

namespace ICD.Catalogue.Controller
{
	public class PresentationController : Singleton<PresentationController>
	{
		[SerializeField]
		private InputActionReference
			_reloadDataAction,
			_hardReloadAction;

#if UNITY_EDITOR
		[SerializeField]
		[Required]
		[ValidateInput("SetSceneNameFromInspector")]
		private UnityEditor.SceneAsset _loadingScene;

		bool SetSceneNameFromInspector(UnityEditor.SceneAsset scene)
		{
			if (scene != null)
			{
				_loadingSceneName = scene.name;
				return true;
			}
			else
			{
				_loadingSceneName = string.Empty;
				return false;
			}
		}
#endif

		[SerializeField]
		[ReadOnly]
		private string _loadingSceneName;
		public override bool dontDestroyOnLoad => false;

		private CancellationTokenSource _lifeCancellation =
			new CancellationTokenSource();

		public void HandleReturnToStartScreen()
		{
			ModelAgregator.instance.LoadData(
				_lifeCancellation.Token);
		}

		void HandleStartScreenActionPerfomance(InputAction.CallbackContext callbackContext)
		{
			HandleReturnToStartScreen();
		}

		void HandleReloadActionPerfomance(InputAction.CallbackContext callbackContext)
		{
			ModelAgregator.instance.LoadData();
		}

		void HandleHardReloadActionPerfomance(InputAction.CallbackContext callbackContext)
		{
			ModelAgregator.instance.LoadData(
				FilesLoadingPolicy.downloadingPrefered);

			SceneManager.LoadScene(
				_loadingSceneName);
		}

		void Awake()
		{
			if (_reloadDataAction)
			{
				_reloadDataAction.action.Enable();
				_reloadDataAction.action.performed += HandleReloadActionPerfomance;
			}

			if (_hardReloadAction)
			{
				_hardReloadAction.action.Enable();
				_hardReloadAction.action.performed += HandleHardReloadActionPerfomance;
			}
		}

		void OnDestroy()
		{
			_lifeCancellation?.Cancel();

			if (_reloadDataAction)
			{
				_reloadDataAction.action.Disable();
				_reloadDataAction.action.performed -= HandleReloadActionPerfomance;
			}

			if (_hardReloadAction)
			{
				_hardReloadAction.action.Disable();
				_hardReloadAction.action.performed -= HandleHardReloadActionPerfomance;
			}
		}
	}
}
