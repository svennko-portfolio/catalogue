using UnityEngine;

namespace ICD.Catalogue.Model.Interfaces
{
    public interface CatalogueItemMainDataInterface
    {
        public uint ID { get; }
        public uint? parentID { get; }
        public uint orderNumber { get; }
        internal string iconURL { get; }
        public Sprite icon { get; internal set; }
    }
}