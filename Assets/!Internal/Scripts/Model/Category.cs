using System;

using UnityEngine;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model
{
	[Serializable]
	public class Category : CatalogueItem, CategoryInterface
	{
		[SerializeField]
		private CategoryMainData _mainData;
		public new CategoryMainDataInterface mainData
		{
			get => _mainData;
			internal set
			{
				if (value == null)
				{
					_mainData = null;
					return;
				}

				var castedData = value as CategoryMainData;
				if (castedData == null)
					castedData =
						new CategoryMainData()
						{
							ID = value.ID,
							parentID = value.parentID,
							orderNumber = value.orderNumber,
							contentType = value.contentType,
						};
				_mainData = castedData;
			}
		}

		protected override CatalogueItemMainDataInterface baseMainData
		{
			get => mainData;
			set => mainData = value as CategoryMainDataInterface;
		}

		public new CategoryLocalizedDataInterface localizedData => base.localizedData as CategoryLocalizedDataInterface;


		public Category(CategoriesLocalizedDataContainerInterface localizedDataContainer) :
			base(localizedDataContainer)
		{ }
	}
}