


namespace ICD.Engine.Core.Animations
{
	public sealed class AnimatorFloatSetter : AnimatorParameterSetter<float>
	{
		protected override float GetParameterFromAnimator() =>
			animator.GetFloat(
				parameterName);
		protected override void SetParameter(float parameterValue)
		{
			animator.SetFloat(
				parameterName,
				parameterValue);
		}
	}
}
