using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Localization;
using Sirenix.OdinInspector;

using ICD.Catalogue.Controller;

namespace ICD.Catalogue.View.Screens
{
	public class MainScreen : PresentationScreen
	{
		[SerializeField]
		[Required]
		private string
			_leftLocaleID = "en-US",
			_rightLocaleID = "ru-RU";

		[SerializeField]
		private UnityEvent _clicked;

		public MainScreen()
		{
			ActivationStateChanged += HandleActivationStateChange;
		}

		void HandleRightClick() =>
			HandleClickForLocale(
				_rightLocaleID);

		void HandleLeftClick() =>
			HandleClickForLocale(
				_leftLocaleID);

		void HandleClickForLocale(string localeID)
		{
			if (LocalizationHelper.SetCurrentLocale(
					localeID))
				_clicked?.Invoke();
		}

		void HandleActivationStateChange(bool newState)
		{
			if (newState)
			{
				InputHandler.instance.LeftClickActionPerformed += HandleLeftClick;
				InputHandler.instance.RightClickActionPerformed += HandleRightClick;
			}
			else
			{
				InputHandler.instance.LeftClickActionPerformed -= HandleLeftClick;
				InputHandler.instance.RightClickActionPerformed -= HandleRightClick;
			}
		}
	}
}
