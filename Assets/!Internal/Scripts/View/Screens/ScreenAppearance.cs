using System;

using UnityEngine;

namespace ICD.Catalogue.View.Screens
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(PresentationScreen))]
    public abstract class ScreenAppearance : MonoBehaviour
    {
        private PresentationScreen _screen;
        private PresentationScreen screen
		{
			get
			{
				if (!_screen)
					_screen = GetComponent<PresentationScreen>();
				return _screen;
			}
		}

		public abstract event Action Appeared;

		public ScreenAppearance()
		{
			Appeared += () => screen.isActive = true;
		}
	}
}