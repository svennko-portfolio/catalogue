﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Video;
using Sirenix.OdinInspector;

using ICD.Catalogue.View.Interfaces;

namespace ICD.Catalogue.View
{
	public class VideoContent : MonoBehaviour, SelectableItemInterface
	{
		[SerializeField]
		[Required]
		[SceneObjectsOnly]
		private VideoPlayer _player;

		[SerializeField]
		[Required]
		[SceneObjectsOnly]
		private RawImage _videoImage;

		[SerializeField]
		private BooleanEvent
			_playingStarted,
			_playingStopped;

		public string videoURL
		{
			get => _player?.url;
			set
			{
				if (!string.IsNullOrWhiteSpace(value))
				{
					_texture = RenderTexture.GetTemporary(
						1920, 1080);
					_player.targetTexture = _texture;
					_videoImage.texture = _texture;
				}
				_player.url = value;
			}
		}

		Rect SelectableItemInterface.rect
		{
			get
			{
				var rectTransorm = transform as RectTransform;
				if (rectTransorm)
					return rectTransorm.rect;

				return Rect.zero;
			}
		}
		Vector2 SelectableItemInterface.pivot
		{
			get
			{
				var rectTransorm = transform as RectTransform;
				if (rectTransorm)
					return rectTransorm.pivot;

				return new Vector2(.5f, .5f);
			}
		}
		Vector2 SelectableItemInterface.anchoredPosition
		{
			get
			{
				var rectTransorm = transform as RectTransform;
				if (rectTransorm)
					return rectTransorm.anchoredPosition;

				return transform.localPosition;
			}
		}
		Vector3 SelectableItemInterface.position => transform.position;
		Matrix4x4 SelectableItemInterface.worldToLocalMatrix => transform.worldToLocalMatrix;
		Matrix4x4 SelectableItemInterface.localToWorldMatrix => transform.localToWorldMatrix;

		private RenderTexture _texture;

		void Play()
		{
			if (_player.isPlaying)
				return;
			else
				ToggleVideo();
		}

		public void ToggleVideo()
		{
			var playOrNot = !_player.isPlaying;
			if (playOrNot)
				_player.Play();
			else
				_player.Stop();

			_playingStarted?.Invoke(playOrNot);
			_playingStopped?.Invoke(!playOrNot);
		}

		void OnDestroy()
		{
			if (_texture)
				RenderTexture.ReleaseTemporary(
					_texture);
		}

		public void Select()
		{
			Play();
		}
	}
}
