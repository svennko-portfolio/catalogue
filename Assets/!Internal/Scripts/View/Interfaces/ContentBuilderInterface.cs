using UnityEngine;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.View.Interfaces
{
    public interface ContentBuilderInterface
    {
        public PageContent Page(CatalogueItemInterface pageData, Transform contentRoot, Selector selector);
    }
}
