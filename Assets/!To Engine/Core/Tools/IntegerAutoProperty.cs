using System;

using UnityEngine;
using UnityEngine.Events;

namespace ICD.Engine.Core.Tools
{
	[Serializable]
	public class IntegerAutoProperty : AutoProperty<int>
	{
		[SerializeField]
		private IntegerEvent _valueChanged;

		public IntegerAutoProperty()
		{
			ValueChanged +=
				value =>
					_valueChanged?.Invoke(
						value);
		}
	}
}
