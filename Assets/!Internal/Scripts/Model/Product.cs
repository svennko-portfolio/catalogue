using System;

using UnityEngine;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model
{
	[Serializable]
	public class Product : CatalogueItem, ProductInterface
	{
		[SerializeField]
		private ProductMainData _mainData;
		public new ProductMainDataInterface mainData
		{
			get => _mainData;
			internal set
			{
				if (value == null)
				{
					_mainData = null;
					return;
				}

				var castedData = value as ProductMainData;
				if (castedData == null)
					castedData =
						new ProductMainData()
						{
							ID = value.ID,
							parentID = value.parentID,
							orderNumber = value.orderNumber
						};
				_mainData = castedData;
			}
		}

		protected override CatalogueItemMainDataInterface baseMainData
		{
			get => mainData;
			set => mainData = value as ProductMainDataInterface;
		}

		public new ProductLocalizedDataInterface localizedData => base.localizedData as ProductLocalizedDataInterface;

		public Product(ProductsLocalizedDataContainerInterface localizedDataContainer) :
			base(localizedDataContainer)
		{ }
	}
}