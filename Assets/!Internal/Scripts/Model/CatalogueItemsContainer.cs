﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using Sirenix.OdinInspector;
using ICD.Engine.Core.CustomAttributes;
using ICD.Engine.Core.Utility;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Data/Containers/Catalogue Items", fileName = "Catalogue Items Container")]
	public class CatalogueItemsContainer : SerializedScriptableObject, WritableCatalogueItemsContainerInterface
	{
		[SerializeField]
		[Required]
		[AssetsOnly]
		[RequireInterface(typeof(CategoriesLocalizedDataContainerInterface))]
		private UnityEngine.Object _categoriesLocalizedDataContainter;
		public CategoriesLocalizedDataContainerInterface categoriesLocalizedDataContainter =>
			_categoriesLocalizedDataContainter as CategoriesLocalizedDataContainerInterface;

		[SerializeField]
		[Required]
		[AssetsOnly]
		[RequireInterface(typeof(ProductsLocalizedDataContainerInterface))]
		private UnityEngine.Object _productsLocalizedDataContainter;
		public ProductsLocalizedDataContainerInterface productsLocalizedDataContainter =>
			_productsLocalizedDataContainter as ProductsLocalizedDataContainerInterface;

		[SerializeField]
		private Dictionary<uint, Category> _categories =
			new Dictionary<uint, Category>(128);

		[SerializeField]
		private Dictionary<uint, Product> _products =
			new Dictionary<uint, Product>(128);

		public IEnumerable<CatalogueItemInterface> all =>
			(_categories.Values as IEnumerable<CatalogueItemInterface>).
			Union(
				_products.Values);

		public CatalogueItemsContainer()
		{
			Application.quitting += ClearData;
		}

		public CategoryInterface GetCategory(uint categoryID)
		{
			_categories.TryGetValue(
				categoryID,
				out Category category);
			return category;
		}

		public ProductInterface GetProduct(uint productID)
		{
			_products.TryGetValue(
				productID,
				out Product product);
			return product;
		}

		public IReadOnlyList<CatalogueItemInterface> GetCategoryChildren(uint? parentCategoryID)
		{
			if (!parentCategoryID.HasValue)
				return
					(from category in _categories
					 where !category.Value.mainData.parentID.HasValue
					 select category.Value).ToArray();
			else
			{
				if (!_categories.TryGetValue(parentCategoryID.Value, out Category parentCategory))
					return null;

				switch (parentCategory.mainData.contentType)
				{
					case CategoryMainDataInterface.ContentType.categories:
						return
							SelectChildrenByParent(
								parentCategoryID.Value,
								_categories.Values);
					case CategoryMainDataInterface.ContentType.products:
						return
							SelectChildrenByParent(
								parentCategoryID.Value,
								_products.Values);
					default:
						return null;
				}
			}
		}

		internal bool TryAddCatalogueItem(CatalogueItemInterface catalogueItem, bool overwriteIfExists = true)
		{
			if (catalogueItem?.mainData == null)
				return false;

			switch (catalogueItem)
			{
				case CategoryInterface category:
					return
						TryAddCategory(
							category,
							overwriteIfExists);
				case ProductInterface product:
					return
						TryAddProduct(
							product,
							overwriteIfExists);
				default:
					return false;
			}
		}

		internal bool TryAddCatalogueItems(IEnumerable<CatalogueItemInterface> catalogueItems, bool overwriteIfExists = true)
		{
			foreach (var item in catalogueItems)
				if (!TryAddCatalogueItem(
					item,
					overwriteIfExists))
					return false;
			return true;
		}

		internal bool TryAddCategory(CategoryInterface category, bool overwriteIfExists = true)
		{
			if (!_categories.ContainsKey(category.mainData.ID) || overwriteIfExists)
			{
				var castedCategory = ConvertCategory(category);

				if (overwriteIfExists)
					_categories[category.mainData.ID] =
						castedCategory;
				else
					_categories.Add(
						castedCategory.mainData.ID,
						castedCategory);

				return true;
			}
			else
				return false;
		}

		internal bool TryAddCategories(IEnumerable<CategoryInterface> categories, bool overwriteIfExists = true)
		{
			foreach (var category in categories)
				if (!TryAddCategory(
					category,
					overwriteIfExists))
					return false;
			return true;
		}

		internal bool TryAddProduct(ProductInterface product, bool overwriteIfExists = true)
		{
			if (!_products.ContainsKey(product.mainData.ID) || overwriteIfExists)
			{
				var castedProduct = ConvertProduct(product);

				if (overwriteIfExists)
					_products[product.mainData.ID] =
						castedProduct;
				else
					_products.Add(
						product.mainData.ID,
						castedProduct);

				return true;
			}
			else
				return false;
		}

		internal bool TryAddProducts(IEnumerable<ProductInterface> products, bool overwriteIfExists = true)
		{
			foreach (var product in products)
				if (!TryAddProduct(
					product,
					overwriteIfExists))
					return false;
			return true;
		}

		[ContextMenu("Clear Data")]
		internal void ClearData()
		{
			foreach (var item in all)
				item.mainData.icon.DestroyByApplicationPlayingState();

			_categories.Clear();
			_products.Clear();
		}

		IReadOnlyList<CatalogueItemInterface> SelectChildrenByParent(uint parentID, IEnumerable<CatalogueItemInterface> selectionItems) =>
			(from item in selectionItems
			 where item.mainData.parentID.HasValue && item.mainData.parentID == parentID
			 orderby item.mainData.orderNumber
			 select item).ToArray();

		Category ConvertCategory(CategoryInterface category)
		{
			if (category == null)
				throw new ArgumentNullException(
					nameof(category));

			var castedCategory = category as Category;
			if (castedCategory == null)
				castedCategory =
					new Category(
						categoriesLocalizedDataContainter)
					{
						mainData = category.mainData
					};
			return castedCategory;
		}

		Product ConvertProduct(ProductInterface product)
		{
			if (product == null)
				throw new ArgumentNullException(
					nameof(product));

			var castedProduct = product as Product;
			if (castedProduct == null)
				castedProduct =
					new Product(
						productsLocalizedDataContainter)
					{
						mainData = product.mainData
					};
			return castedProduct;
		}

		bool WritableCatalogueItemsContainerInterface.TryAddCatalogueItem(CatalogueItemInterface catalogueItem, bool overwriteIfExists) =>
			TryAddCatalogueItem(
				catalogueItem,
				overwriteIfExists);
		bool WritableCatalogueItemsContainerInterface.TryAddCatalogueItems(IEnumerable<CatalogueItemInterface> catalogueItems, bool overwriteIfExists) =>
			TryAddCatalogueItems(
				catalogueItems,
				overwriteIfExists);
		bool WritableCatalogueItemsContainerInterface.TryAddCategory(CategoryInterface category, bool overwriteIfExists) =>
			TryAddCategory(
				category,
				overwriteIfExists);
		bool WritableCatalogueItemsContainerInterface.TryAddCategories(IEnumerable<CategoryInterface> categories, bool overwriteIfExists) =>
			TryAddCategories(
				categories,
				overwriteIfExists);
		bool WritableCatalogueItemsContainerInterface.TryAddProduct(ProductInterface product, bool overwriteIfExists) =>
			TryAddProduct(
				product,
				overwriteIfExists);
		bool WritableCatalogueItemsContainerInterface.TryAddProducts(IEnumerable<ProductInterface> products, bool overwriteIfExists) =>
			TryAddProducts(
				products,
				overwriteIfExists);
		void WritableCatalogueItemsContainerInterface.ClearData() =>
			ClearData();
	}
}