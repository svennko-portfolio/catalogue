﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Localization;
using Sirenix.OdinInspector;
using ICD.Engine.Core.CustomAttributes;

using ICD.Catalogue.Model.DataLoading.Interfaces;
using ICD.Catalogue.Model.Interfaces;
using FilesLoadingPolicy = ICD.Catalogue.Model.DataLoading.Interfaces.DataLoaderInterface.FilesLoadingPolicy;

namespace ICD.Catalogue.Model.DataLoading
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Data/Data Loader", fileName = "Data Loader")]
	public class DataLoader : ScriptableObject, DataLoaderInterface
	{
		[SerializeField]
		[Required]
		[RequireInterface(typeof(DataBaseAPI))]
		private UnityEngine.Object _dataBaseAPI;
		private DataBaseAPI dataBaseAPI => _dataBaseAPI as DataBaseAPI;

		[SerializeField]
		private FilesLoadingPolicy _defaultFilesLoadingPolicy =
			FilesLoadingPolicy.checkLastChangesTime;
		internal FilesLoadingPolicy defaultFilesLoadingPolicy
		{
			get => _defaultFilesLoadingPolicy;
			set => _defaultFilesLoadingPolicy = value;
		}

		[SerializeField]
		[ValidateInput("InitializeFullCahceDirectory")]
		private string _cacheDirectoryPath = string.Empty;

		private DirectoryInfo _cacheDirectory;
		private DirectoryInfo cacheDirectory
		{
			get
			{
				if (_cacheDirectory == null)
					InitializeFullCahceDirectory(
						_cacheDirectoryPath);

				return _cacheDirectory;
			}
		}

#if UNITY_EDITOR
		[FoldoutGroup("Editor Only")]
		[SerializeField]
		[ValidateInput("InitializeFullCahceDirectory")]
		private string _rootPath;
#endif

		private IReadOnlyDictionary<FilesLoadingPolicy, FilesLoaderInterface> _loaders;
		private IReadOnlyDictionary<FilesLoadingPolicy, FilesLoaderInterface> loaders
		{
			get
			{
				if (_loaders == null)
					_loaders =
						new Dictionary<FilesLoadingPolicy, FilesLoaderInterface>()
						{
							{ FilesLoadingPolicy.cacheOnly, new CacheOnlyLoader() },
							{ FilesLoadingPolicy.cachePrefered, new CachePreferedLoader() },
							{ FilesLoadingPolicy.checkLastChangesTime, new ChangesTimeBasedLoader(dataBaseAPI) },
							{ FilesLoadingPolicy.downloadingPrefered, new DownloadingPreferdLoader() },
							{ FilesLoadingPolicy.downloadOnly, new DownloadingOnlyLoader() },
						};
				return _loaders;
			}
		}

		public DataLoader()
		{
			Application.quitting += Dispose;
		}

		internal async Task LoadMainData(WritableCatalogueItemsContainerInterface catalogueItemsContainer, WritableCategoriesLocalizedDataContainerInterface categoriesLocalizedDataContainer, WritableProductsLocalizedDataContainerInterface productsLocalizedDataContainer, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(
					cancellationToken,
					nameof(LoadMainData)) ||
				!dataBaseAPI.isLoggedIn &&
				!await dataBaseAPI.LogIn(
						DataBaseCredentials.FromSettings(),
						cancellationToken))
				return;

			await LoadCategories(
				catalogueItemsContainer,
				categoriesLocalizedDataContainer,
				cancellationToken);

			await LoadProducts(
				catalogueItemsContainer,
				productsLocalizedDataContainer,
				cancellationToken);
		}

		internal async Task LoadMediaData(CatalogueItemsContainerInterface catalogueItemsContainer, CategoriesLocalizedDataContainerInterface categoriesLocalizedDataContainer, ProductsLocalizedDataContainerInterface productsLocalizedDataContainer, FilesLoadingPolicy filesLoadingPolicy, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(
					cancellationToken,
					nameof(LoadMediaData)) ||
				!dataBaseAPI.isLoggedIn &&
				!await dataBaseAPI.LogIn(
						DataBaseCredentials.FromSettings(),
						cancellationToken))
				return;

			var catalogueItems = catalogueItemsContainer.all;

			var iconsLoadingTasks =
				new Dictionary<CatalogueItemInterface, Task<FileInfo>>(
					catalogueItems.Count());
			var logosLoadingTasks =
				new Dictionary<CatalogueItemLocalizedDataInterface, Task<FileInfo>>(
					catalogueItems.Count());
			var photosLoadingTasks =
				new Dictionary<ProductLocalizedDataInterface, List<Task<FileInfo>>>(
					catalogueItems.Count());

			var loader = loaders[filesLoadingPolicy];

			foreach (var item in catalogueItems)
			{
				if (item?.mainData != null)
					iconsLoadingTasks.Add(
						item,
						loader.LoadFile(
							new Uri(
								item.mainData.iconURL),
							cacheDirectory,
							cancellationToken));

				if (item?.localizedData != null)
					logosLoadingTasks.Add(
						item.localizedData,
						loader.LoadFile(
							new Uri(
								item.mainData.iconURL),
							cacheDirectory,
							cancellationToken));
			}

			foreach (var categoryLocalizedData in categoriesLocalizedDataContainer.all)
			{
				if (categoryLocalizedData == null ||
					string.IsNullOrWhiteSpace(categoryLocalizedData.logoURL))
					continue;

				logosLoadingTasks.Add(
					categoryLocalizedData,
					loader.LoadFile(
						new Uri(
							categoryLocalizedData.logoURL),
						cacheDirectory,
						cancellationToken));
			}

			foreach (var productLocalizedData in productsLocalizedDataContainer.all)
			{
				if (productLocalizedData == null)
					continue;

				if (!string.IsNullOrWhiteSpace(productLocalizedData.logoURL))
					logosLoadingTasks.Add(
						productLocalizedData,
						loader.LoadFile(
							new Uri(
								productLocalizedData.logoURL),
							cacheDirectory,
							cancellationToken));

				if (productLocalizedData.photosURLs == null ||
					productLocalizedData.photosURLs.Count == 0)
					continue;

				var productPhotosLoadingTasks =
					new List<Task<FileInfo>>(
						productLocalizedData.photosURLs.Count);

				foreach (var photoURL in productLocalizedData.photosURLs)
					if (!string.IsNullOrWhiteSpace(photoURL))
						productPhotosLoadingTasks.Add(
							loader.LoadFile(
								new Uri(
									photoURL),
								cacheDirectory,
								cancellationToken));

				if (productPhotosLoadingTasks.Count > 0)
					photosLoadingTasks.Add(
						productLocalizedData,
						productPhotosLoadingTasks);
			}

			await
				Task.WhenAll(
					iconsLoadingTasks.Values.Union(
						logosLoadingTasks.Values).
					Union(
						from productPhotosLoadingTasks in photosLoadingTasks.Values
						from photoLoadingTask in productPhotosLoadingTasks
						select photoLoadingTask));

			foreach (var iconLoadingTask in iconsLoadingTasks)
				if (iconLoadingTask.Key?.mainData != null &&
					iconLoadingTask.Value.IsCompletedSuccessfully)
				{
					iconLoadingTask.Key.mainData.icon =
						LoadImage(
							iconLoadingTask.Value.Result);
					await Task.Yield();
				}

			foreach (var logoLoadingTask in logosLoadingTasks)
				if (logoLoadingTask.Key != null &&
					logoLoadingTask.Value.IsCompletedSuccessfully)
				{
					logoLoadingTask.Key.logo =
						LoadImage(
							logoLoadingTask.Value.Result);
					await Task.Yield();
				}

			foreach (var productPhotosLoadingTasks in photosLoadingTasks)
			{
				if (productPhotosLoadingTasks.Key == null ||
					productPhotosLoadingTasks.Value == null ||
					productPhotosLoadingTasks.Value.Count == 0)
					continue;

				var photos =
					new List<Sprite>(
						productPhotosLoadingTasks.Value.Count);

				foreach (var photoLoadingTask in productPhotosLoadingTasks.Value)
				{
					photos.Add(
						LoadImage(
							photoLoadingTask.Result));
					await Task.Yield();
				}

				productPhotosLoadingTasks.Key.photos =
					photos.ToArray();
			}
		}

		async Task LoadCategories(WritableCatalogueItemsContainerInterface catalogueItemsContainer, WritableCategoriesLocalizedDataContainerInterface categoriesLocalizedDataContainer, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(cancellationToken))
				return;

			IEnumerable<CategoryMainDataInterface> categoriesMainData = null;

			try
			{
				categoriesMainData =
					await dataBaseAPI.GetAllCategories(
						cancellationToken);
			}
			catch (OperationCanceledException) { return; }
			catch (Exception exception)
			{
				Debug.LogError(
					$"Faild to load main data for all categories:\n" +
					exception);
				return;
			}

			if (categoriesMainData == null)
				return;

			var loadingTasks = new List<Task>(categoriesMainData.Count());

			foreach (var categoryMainData in categoriesMainData)
			{
				catalogueItemsContainer.TryAddCategory(
					new Category(
						categoriesLocalizedDataContainer)
					{
						mainData = categoryMainData
					});

				foreach (var localeID in LocalizationHelper.availableLocales)
					loadingTasks.Add(
						LoadCategoryLocalizedData(
							categoryMainData.ID,
							localeID,
							categoriesLocalizedDataContainer,
							cancellationToken));
			}

			await Task.WhenAll(
				loadingTasks);
		}

		async Task LoadCategoryLocalizedData(uint categoryID, string localeID, WritableCategoriesLocalizedDataContainerInterface categoriesLocalizedDataContainer, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(cancellationToken))
				return;

			try
			{
				var localizedData =
					await dataBaseAPI.GetCategoryLocalizedData(
						categoryID,
						localeID,
						cancellationToken);

				if (!categoriesLocalizedDataContainer.TryAddLocalizedData(
						categoryID,
						localeID,
						localizedData))
					Debug.Log(
						$"Failed to add category {{ID: {categoryID}; Locale: {localeID}}} localized data to container");
			}
			catch (OperationCanceledException) { return; }
			catch (Exception exception)
			{
				Debug.LogError(
					$"Faild to load category {{ID: {categoryID}; Locale: {localeID}}} localized data:\n" +
					exception);
			}
		}

		async Task LoadProducts(WritableCatalogueItemsContainerInterface catalogueItemsContainer, WritableProductsLocalizedDataContainerInterface productsLocalizedDataContainer, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(cancellationToken))
				return;

			IEnumerable<ProductMainDataInterface> productsMainData = null;

			try
			{
				productsMainData =
					await dataBaseAPI.GetAllProducts(
						cancellationToken);
			}
			catch (OperationCanceledException) { return; }
			catch (Exception exception)
			{
				Debug.LogError(
					$"Faild to load main data for all products:\n" +
					exception);
				return;
			}

			if (productsMainData == null)
				return;

			var loadingTasks = new List<Task>(productsMainData.Count());

			foreach (var productMainData in productsMainData)
			{
				catalogueItemsContainer.TryAddProduct(
					new Product(
						productsLocalizedDataContainer)
					{
						mainData = productMainData
					});

				foreach (var localeID in LocalizationHelper.availableLocales)
					loadingTasks.Add(
						LoadProductLocalizedData(
							productMainData.ID,
							localeID,
							productsLocalizedDataContainer,
							cancellationToken));
			}

			await Task.WhenAll(
				loadingTasks);
		}

		async Task LoadProductLocalizedData(uint productID, string localeID, WritableProductsLocalizedDataContainerInterface productsLocalizedDataContainer, CancellationToken cancellationToken = default)
		{
			if (CheckCancellation(cancellationToken))
				return;

			try
			{
				var localizedData =
					await dataBaseAPI.GetProductLocalizedData(
						productID,
						localeID,
						cancellationToken);

				if (!productsLocalizedDataContainer.TryAddLocalizedData(
						productID,
						localeID,
						localizedData))
					Debug.Log(
						$"Failed to add product {{ID: {productID}; Locale: {localeID}}} localized data to container");
			}
			catch (OperationCanceledException) { return; }
			catch (Exception exception)
			{
				Debug.LogError(
					$"Faild to load product {{ID: {productID}; Locale: {localeID}}} localized data:\n" +
					exception);
			}
		}

		Sprite LoadImage(FileInfo imageFile)
		{
			if (imageFile == null || !imageFile.Exists)
				return null;

			try
			{
				if (imageFile == null || !imageFile.Exists)
					return null;

				var texture =
					new Texture2D(1, 1);
				texture.LoadImage(
					File.ReadAllBytes(
						imageFile.FullName));

				return
					Sprite.Create(
						texture,
						new Rect(
							0,
							0,
							texture.width,
							texture.height),
						new Vector2(
							.5f,
							.5f));
			}
			catch (Exception exception)
			{
				Debug.LogError(
					$"Failed to load image from \"{imageFile}\":\n" +
					exception);
				return null;
			}
		}

		bool CheckCancellation(CancellationToken cancellationToken, string operationName = null)
		{
			if (cancellationToken.IsCancellationRequested)
			{
				Debug.Log(
					$"Operation {operationName} cancelled");
				return true;
			}
			return false;
		}

		bool InitializeFullCahceDirectory(string newPathPart)
		{
			var pathParts =
				new List<string>()
				{
					Application.dataPath
				};

#if UNITY_EDITOR
			if (!string.IsNullOrWhiteSpace(_rootPath))
				pathParts.Add(
					_rootPath);
#endif

			if (!string.IsNullOrWhiteSpace(_cacheDirectoryPath))
				pathParts.Add(
					_cacheDirectoryPath);

			_cacheDirectory =
				new DirectoryInfo(
					Path.Combine(
						pathParts.ToArray()));

			if (!_cacheDirectory.Exists)
				_cacheDirectory =
					Directory.CreateDirectory(
						_cacheDirectory.FullName);

			return true;
		}

		[ContextMenu("Remove Cache")]
		internal void RemoveCache()
		{
			if (_cacheDirectory != null && _cacheDirectory.Exists)
				Directory.Delete(
					_cacheDirectory.FullName,
					true);
		}

		internal void Dispose()
		{
			_cacheDirectory = null;
		}

		FilesLoadingPolicy DataLoaderInterface.defaultFilesLoadingPolicy
		{
			get => _defaultFilesLoadingPolicy;
			set => _defaultFilesLoadingPolicy = value;
		}

		Task DataLoaderInterface.LoadMainData(WritableCatalogueItemsContainerInterface catalogueItemsContainer, WritableCategoriesLocalizedDataContainerInterface categoriesLocalizedDataContainer, WritableProductsLocalizedDataContainerInterface productsLocalizedDataContainer, CancellationToken cancellationToken) =>
			LoadMainData(
				catalogueItemsContainer,
				categoriesLocalizedDataContainer,
				productsLocalizedDataContainer,
				cancellationToken);
		Task DataLoaderInterface.LoadMediaData(CatalogueItemsContainerInterface catalogueItemsContainer, CategoriesLocalizedDataContainerInterface categoriesLocalizedDataContainer, ProductsLocalizedDataContainerInterface productsLocalizedDataContainer, FilesLoadingPolicy filesLoadingPolicy, CancellationToken cancellationToken = default) =>
			LoadMediaData(
				catalogueItemsContainer,
				categoriesLocalizedDataContainer,
				productsLocalizedDataContainer,
				filesLoadingPolicy,
				cancellationToken);

		void DataLoaderInterface.RemoveCache() =>
			RemoveCache();
	}
}