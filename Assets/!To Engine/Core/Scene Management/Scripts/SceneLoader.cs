﻿
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace ICD.Engine.Core.SceneManagment
{
	public class SceneLoader : MonoBehaviour
	{
		const int ASYNC_PAUSE_DURATION = 15;
		const float READY_SCENE_PROGRESS = .9f;

		private static AsyncOperation _loadingOperation;

		[SerializeField, Min(0)]
		private float minimalLoadingDelay;

		[Space]
		public UnityEvent
			_sceneLoadingStarted,
			_sceneLoaded,
			_sceneOpened;

		[SerializeField]
		private bool _allowSceneActivation;

		private float _overridedMinimalLoadingDelay = -1;
		public float _loadingProgress { get; private set; }
		async void AsyncSceneLoading(float openingDelay)
		{
			DontDestroyOnLoad(gameObject);
			_allowSceneActivation = _loadingOperation.allowSceneActivation = false;

			_sceneLoadingStarted?.Invoke();
			var startTime = Time.realtimeSinceStartup;

			do
				await Task.Delay(ASYNC_PAUSE_DURATION);
			while (_loadingOperation.progress < READY_SCENE_PROGRESS);

			_sceneLoaded?.Invoke();

			if (!_allowSceneActivation)
			{
				while (!_allowSceneActivation && Time.realtimeSinceStartup - startTime < openingDelay)
					await Task.Delay(ASYNC_PAUSE_DURATION);


				_allowSceneActivation = false;

				if (_overridedMinimalLoadingDelay > 0)
					_overridedMinimalLoadingDelay = -1;
			}

			if (_loadingOperation != null)
			{
				_loadingOperation.allowSceneActivation = true;

				while (!_loadingOperation.isDone)
					await Task.Delay(ASYNC_PAUSE_DURATION);

				_loadingOperation = null;
			}

			_sceneOpened?.Invoke();

			System.GC.Collect(0, System.GCCollectionMode.Optimized);
		}
		bool IsLoadingAvailable()
		{
			if (_loadingOperation != null)
			{
				//Debug.LogError("<color=green>Some scene already loading</color>");
				return false;
			}
			return true;
		}
		void LoadSceneAsync(object sceneIdentificator)
		{
			if (!IsLoadingAvailable()) return;

			switch (sceneIdentificator)
			{
				case int sceneID:
					_loadingOperation = SceneManager.LoadSceneAsync(sceneID);
					break;
				case string sceneName:
					_loadingOperation = SceneManager.LoadSceneAsync(sceneName);
					break;
				default:
					Debug.LogError(
						$"Scene ID must be a {typeof(int)} or a {typeof(string)}.\n" +
						$"Current type: {sceneIdentificator.GetType()}");
					break;
			}

			AsyncSceneLoading(_overridedMinimalLoadingDelay > 0 ? _overridedMinimalLoadingDelay : minimalLoadingDelay);
		}
		public void LoadSceneAsync(int sceneIndex) => LoadSceneAsync(sceneIndex as object);
		public void LoadSceneAsync(string sceneName) => LoadSceneAsync(sceneName as object);
		public void OverrideNextOneLoadingDelay(float _delay) => _overridedMinimalLoadingDelay = _delay;

		[ContextMenu("Activate immediate")]
		public void OpenImmediateOrWhenLoaded()
		{
			_allowSceneActivation = true;
		}
		public void LoadScene(int sceneIndex) => SceneManager.LoadScene(sceneIndex);
		public void LoadScene(string scenename) => SceneManager.LoadScene(scenename);
	}
}