using System.Collections.Generic;

using UnityEngine;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Utility;

using ICD.Catalogue.Model;
using ICD.Catalogue.Model.Interfaces;
using ICD.Catalogue.View.Interfaces;

namespace ICD.Catalogue.View
{
	public class ListContent : PageContent
	{
		[SerializeField]
		[Required]
		[AssetsOnly]
		private ItemCard _itemPrefab;

		[SerializeField]
		[SceneObjectsOnly]
		private Transform _itemsRoot;

		[SerializeField]
		[ReadOnly]
		private List<ItemCard> _items =
			new List<ItemCard>();
		public IReadOnlyList<SelectableItemInterface> items =>
			_items;

		private CatalogueItemInterface _data;
		public override CatalogueItemInterface data => _data;

		public override void Construct(CatalogueItemInterface data)
		{
			ClearData();

			foreach (var itemData in ModelAgregator.instance.catalogueItemsContainer.GetCategoryChildren(data?.mainData.ID))
			{
				var newItem =
					Instantiate(
						_itemPrefab,
						_itemsRoot);
				newItem.data = itemData;
				newItem.Selected += HandleItemSelection;
				_items.Add(
					newItem);
			}

			_data = data;
		}

		void ClearData()
		{
			foreach (var item in _items)
			{
				if (!item)
					continue;

				item.Selected -= HandleItemSelection;
				item.DestroyByApplicationPlayingState();
			}
			_items.Clear();

			_data = null;
		}

		void HandleItemSelection(ItemCard item)
		{
			owner.GoForward(
				item.data);
		}
	}
}