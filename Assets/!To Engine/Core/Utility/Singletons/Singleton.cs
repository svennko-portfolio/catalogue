
using UnityEngine;

namespace ICD.Engine.Core.Singletons
{
	[RequireComponent(typeof(InstanceChecker))]
	[DisallowMultipleComponent]
	public abstract class Singleton : MonoBehaviour
	{
		public enum InstantiateMode
		{
			destroyNew,
			destroyOld
		}
		[SerializeField]
		private InstantiateMode _instantiateMode;
		protected InstantiateMode instantiateMode => _instantiateMode;

		public abstract bool dontDestroyOnLoad { get; }
		protected internal virtual void OnInstanceStart()
		{
			if (dontDestroyOnLoad)
#if UNITY_EDITOR
				if (Application.isPlaying)
#endif
					DontDestroyOnLoad(gameObject);
		}
		protected internal abstract void OnInstanceDestroy();
	}

	public abstract class Singleton<FinalType> : Singleton
		where FinalType : Singleton<FinalType>
	{
		private static FinalType _instance;

		private static object _instanceLocker = new object();
		public static FinalType instance
		{
			get
			{
				lock (_instanceLocker)
					if (!_instance)
					{
						var otherInstances = FindObjectsOfType<FinalType>();
						if (otherInstances != null && otherInstances.Length > 0)
						{
							_instance = otherInstances[0];
							for (int i = 1; i < otherInstances.Length; i++)
								DestroyExcessInstance(otherInstances[i]);
						}
						else
							_instance = new GameObject(typeof(FinalType).Name).AddComponent<FinalType>();
					}
				return _instance;
			}
		}
		static void DestroyExcessInstance(FinalType instance)
		{
			if (!instance)
				return;

			Object destroyTarget = instance;
			if (instance.gameObject != _instance.gameObject)
				destroyTarget = instance.gameObject;

#if UNITY_EDITOR
			if (!Application.isPlaying)
				DestroyImmediate(destroyTarget);
			else
#endif
				Destroy(destroyTarget);
		}
		protected internal sealed override void OnInstanceStart()
		{
			base.OnInstanceStart();

			lock (_instanceLocker)
			{
				if (!_instance || _instance == this)
				{
					_instance = this as FinalType;
					return;
				}

				switch (instantiateMode)
				{
					case InstantiateMode.destroyNew:
						DestroyExcessInstance(this as FinalType);
						break;
					case InstantiateMode.destroyOld:
						var destroyTarget = _instance;
						_instance = this as FinalType;
						DestroyExcessInstance(destroyTarget);
						break;
				}
			}
		}
		protected internal sealed override void OnInstanceDestroy()
		{
			if (_instance == this)
				_instance = null;
		}
	}
}
