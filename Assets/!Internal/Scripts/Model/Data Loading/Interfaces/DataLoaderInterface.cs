﻿using System.Threading;
using System.Threading.Tasks;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model.DataLoading.Interfaces
{
	internal interface DataLoaderInterface
	{
		public enum FilesLoadingPolicy
		{
			cacheOnly,
			cachePrefered,
			checkLastChangesTime,
			downloadingPrefered,
			downloadOnly
		}
		public FilesLoadingPolicy defaultFilesLoadingPolicy { get; set; }
		public Task LoadMainData(WritableCatalogueItemsContainerInterface catalogueItemsContainer, WritableCategoriesLocalizedDataContainerInterface categoriesLocalizedDataContainer, WritableProductsLocalizedDataContainerInterface productsLocalizedDataContainer, CancellationToken cancellationToken = default);
		public Task LoadMediaData(CatalogueItemsContainerInterface catalogueItemsContainer, CategoriesLocalizedDataContainerInterface categoriesLocalizedDataContainer, ProductsLocalizedDataContainerInterface productsLocalizedDataContainer, FilesLoadingPolicy filesLoadingPolicy, CancellationToken cancellationToken = default);
		public void RemoveCache();
	}
}
