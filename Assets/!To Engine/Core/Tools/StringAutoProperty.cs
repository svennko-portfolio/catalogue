using System;

using UnityEngine;
using UnityEngine.Events;

namespace ICD.Engine.Core.Tools
{
	[Serializable]
	public class StringAutoProperty : AutoProperty<string>
	{
		[SerializeField]
		private StringEvent _valueChanged;

		public StringAutoProperty()
		{
			ValueChanged +=
				value =>
					_valueChanged?.Invoke(
						value);
		}
	}
}