using System;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model
{
	[Serializable]
	public class ProductMainData : CatalogueItemMainData, ProductMainDataInterface
	{ }
}