using System.Collections.Generic;

namespace ICD.Catalogue.Model.Interfaces
{
	public interface ProductsLocalizedDataContainerInterface : CatalogueItemsLocalizedDataContainerInterface
	{
		public new IEnumerable<ProductLocalizedDataInterface> all { get; }
		public bool TryGetLocalizedData(uint productID, string localeID, out ProductLocalizedDataInterface localizedData);
		public bool TryGetLocalizedDataForCurrentLocale(uint productID, out ProductLocalizedDataInterface localizedData);
	}

	internal interface WritableProductsLocalizedDataContainerInterface : ProductsLocalizedDataContainerInterface, WritableCatalogueItemsLocalizedDataContainerInterface
	{
		public bool TryAddLocalizedData(uint productID, string localeID, ProductLocalizedDataInterface localizedData, bool owerwriteIfExists = true);
		public new void ClearData();
	}
}