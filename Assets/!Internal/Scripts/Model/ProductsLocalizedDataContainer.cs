using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Localization;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Utility;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.Model
{
	[CreateAssetMenu(menuName = "ICD/Catalogue/Data/Containers/Products Localized Data", fileName = "Products Localized Data Container")]
	public class ProductsLocalizedDataContainer : SerializedScriptableObject, WritableProductsLocalizedDataContainerInterface
	{
		[SerializeField]
		private Dictionary<LocalizedDataKey, ProductLocalizedData> _productsLocalizedData =
			new Dictionary<LocalizedDataKey, ProductLocalizedData>();

		public IEnumerable<ProductLocalizedDataInterface> all => _productsLocalizedData.Values;
		IEnumerable<CatalogueItemInterface> CatalogueItemsLocalizedDataContainerInterface.all =>
			all as IEnumerable<CatalogueItemInterface>;

		public ProductsLocalizedDataContainer()
		{
			Application.quitting += ClearData;
		}

		public bool TryGetLocalizedData(uint productID, string localeID, out ProductLocalizedDataInterface localizedData)
		{
			localizedData = null;
			if (_productsLocalizedData.TryGetValue(
					new LocalizedDataKey(
						productID,
						localeID),
					out ProductLocalizedData productsLocalizedData))
			{
				localizedData = productsLocalizedData;
				return true;
			}
			return false;
		}

		public bool TryGetLocalizedDataForCurrentLocale(uint productID, out ProductLocalizedDataInterface localizedData) =>
			TryGetLocalizedData(
				productID,
				LocalizationHelper.currentLocaleID,
				out localizedData);

		public bool TryAddLocalizedData(uint productID, string localeID, ProductLocalizedDataInterface localizedData, bool owerwriteIfExists = true)
		{
			if (localizedData == null)
				return false;

			var key =
				new LocalizedDataKey(
					productID,
					localeID);

			if (_productsLocalizedData.ContainsKey(key) && !owerwriteIfExists)
				return false;
			else
			if (!_productsLocalizedData.ContainsKey(key))
			{
				_productsLocalizedData.Add(
					key,
					localizedData is ProductLocalizedData ?
						localizedData as ProductLocalizedData :
						new ProductLocalizedData()
						{
							name = localizedData.name,
							hideNameInHeader = localizedData.hideNameInHeader,
							logoURL = localizedData.logoURL,
							logo = localizedData.logo,
							shortDescription = localizedData.shortDescription,
							fullDescription = localizedData.fullDescription,
							photos = localizedData.photos,
							photosURLs = localizedData.photosURLs,
							videosURLs = localizedData.videosURLs,
							productWebsiteQRCode = localizedData.productWebsiteQRCode,
							productWebsiteQRCodeURL = localizedData.productWebsiteQRCodeURL,
							managerQRCode = localizedData.managerQRCode,
							managerQRCodeURL = localizedData.managerQRCodeURL,
						});
				return true;
			}
			else
			if (owerwriteIfExists)
			{
				_productsLocalizedData[key] =
					localizedData is ProductLocalizedData ?
						localizedData as ProductLocalizedData :
						new ProductLocalizedData()
						{
							name = localizedData.name,
							hideNameInHeader = localizedData.hideNameInHeader,
							logoURL = localizedData.logoURL,
							logo = localizedData.logo,
							shortDescription = localizedData.shortDescription,
							fullDescription = localizedData.fullDescription,
							photos = localizedData.photos,
							photosURLs = localizedData.photosURLs,
							videosURLs = localizedData.videosURLs,
							productWebsiteQRCode = localizedData.productWebsiteQRCode,
							productWebsiteQRCodeURL = localizedData.productWebsiteQRCodeURL,
							managerQRCode = localizedData.managerQRCode,
							managerQRCodeURL = localizedData.managerQRCodeURL,
						};
				return true;
			}

			return false;
		}

		[ContextMenu("Clear Data")]
		public void ClearData()
		{
			foreach (var productLocalizedData in _productsLocalizedData.Values)
			{
				productLocalizedData.logo.DestroyByApplicationPlayingState();

				if (productLocalizedData.photos != null && productLocalizedData.photos.Count > 0)
					foreach (var photo in productLocalizedData.photos)
						photo.DestroyByApplicationPlayingState();
			}

			_productsLocalizedData.Clear();
		}

		bool CheckCatalogueItemType(CatalogueItemLocalizedDataInterface catalogueItemLocalizedData)
		{
			if (!(catalogueItemLocalizedData is CategoryLocalizedDataInterface))
				throw new InvalidCastException(
					$"Value must implement {nameof(CategoryLocalizedDataInterface)} interface");

			return true;
		}


		bool CatalogueItemsLocalizedDataContainerInterface.TryGetLocalizedData(uint itemID, string localeID, out CatalogueItemLocalizedDataInterface localizedData)
		{
			localizedData = null;
			if (TryGetLocalizedData(
						itemID,
						localeID,
						out ProductLocalizedDataInterface productLocalizedData))
			{
				localizedData = productLocalizedData;
				return true;
			}
			return false;
		}

		bool CatalogueItemsLocalizedDataContainerInterface.TryGetLocalizedDataForCurrentLocale(uint itemID, out CatalogueItemLocalizedDataInterface localizedData) =>
			(this as CatalogueItemsLocalizedDataContainerInterface).TryGetLocalizedData(
				itemID,
				LocalizationHelper.currentLocaleID,
				out localizedData);

		bool WritableCatalogueItemsLocalizedDataContainerInterface.TryAddLocalizedData(uint itemID, string localeID, CatalogueItemLocalizedDataInterface localizedData, bool owerwriteIfExists) =>
			TryAddLocalizedData(
				itemID,
				localeID,
				localizedData as ProductLocalizedDataInterface,
				owerwriteIfExists);
	}
}