using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

using ICD.Catalogue.Model.Interfaces;

namespace ICD.Catalogue.View
{
	public class ProductContent : PageContent
	{
		[SerializeField]
		private TMP_Text
			_nameField,
			_shortDescriptionField,
			_fullDescriptionField;

		[SerializeField]
		[Required]
		[SceneObjectsOnly]
		private Transform
			_mainMediaContentContainer,
			_otherMediaContentContainer;

		[SerializeField]
		[Required]
		[SceneObjectsOnly]
		private Image
			_websiteQR,
			_managerQR;

		[SerializeField]
		[Required]
		[AssetsOnly]
		private VideoContent _videoContentPrefab;

		[SerializeField]
		[Required]
		[AssetsOnly]
		private PhotoContent _photoPrefab;

		private ProductInterface _productData;
		public override CatalogueItemInterface data => _productData;

		public override void Construct(CatalogueItemInterface data)
		{
			_productData = data as ProductInterface;
			if (_productData == null)
				return;

			SetText(
				_nameField,
				_productData.localizedData?.name);
			SetText(
				_shortDescriptionField,
				_productData.localizedData?.shortDescription);
			SetText(
				_fullDescriptionField,
				_productData.localizedData?.fullDescription);

			var videos = _productData.localizedData?.videosURLs;
			var photos =
					_productData.localizedData?.photos != null ?
						new List<Sprite>(
							_productData.localizedData.photos) :
						null;

			_mainMediaContentContainer.gameObject.SetActive(false);
			if (videos != null && videos.Count > 0)
			{
				for (int v = 0; v < videos.Count; v++)
					if (!string.IsNullOrWhiteSpace(videos[v]))
					{
						Instantiate(
							_videoContentPrefab,
							_mainMediaContentContainer).videoURL = videos[0];
						_mainMediaContentContainer.gameObject.SetActive(true);

						break;
					}
			}

			if (!_mainMediaContentContainer.gameObject.activeSelf &&
				photos != null && photos.Count > 0)
			{
				for (int p = 0; p < videos.Count; p++)
					if (photos[p])
					{
						Instantiate(
							_photoPrefab,
							_mainMediaContentContainer).photo = photos[0];
						photos.RemoveAt(0);
						_mainMediaContentContainer.gameObject.SetActive(true);

						break;
					}
			}

			if (photos != null)
			{
				_otherMediaContentContainer.gameObject.SetActive(photos.Count > 0);
				foreach (var photo in photos)
					if (photo)
						Instantiate(
							_photoPrefab,
							_otherMediaContentContainer).photo = photo;
			}

			SetImage(
				_websiteQR,
				_productData.localizedData?.productWebsiteQRCode);
			SetImage(
				_managerQR,
				_productData.localizedData?.managerQRCode);
		}

		void SetText(TMP_Text textField, string text)
		{
			if (!textField)
				return;

			textField.text = text;
			textField.gameObject.SetActive(
				!string.IsNullOrEmpty(
					text));
		}

		void SetImage(Image image, Sprite sprite)
		{
			if (!image)
				return;

			image.sprite = sprite;
			image.gameObject.SetActive(
				sprite);
		}
	}
}